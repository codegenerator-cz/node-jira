const path = require('path')

module.exports = {
  output: {
    filename: 'jira.js',
    library: 'JiraKit'
  },
  resolve: {
    alias: {
      deepmerge$: path.resolve('node_modules/deepmerge/dist/umd.js')
    }
  }
}
