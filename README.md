[![version:@latest](https://img.shields.io/npm/v/bitbucket.svg?style=for-the-badge)](https://www.npmjs.com/package/@atlassian/jira)

# Jira.js

JIRA API client for Browser and Node.js

This is a port of the excellent bitbucket library by Munif Tanjim:
[https://github.com/MunifTanjim/node-bitbucket](https://github.com/MunifTanjim/node-bitbucket)

JIRA API docs: [https://developer.atlassian.com/cloud/jira/platform/rest/v2/](https://developer.atlassian.com/cloud/jira/platform/rest/v2/)

## Installation

via **npm**:

```sh
$ npm install @atlassian/jira --save
```

via **yarn**:

```sh
$ yarn add @atlassian/jira
```

## Usage

### Node

```js
const JIRA = require('@atlassian/jira')

const jira = new JIRA()
```

#### Client Options

You can set the APIs' `baseUrl` and modify some behaviors (e.g. request timeout etc.) by passing a clientOptions object to the `JIRA` constructor.

```js
const clientOptions = {
  baseUrl: 'https://api.atlassian.com/ex/jira/${cloudId}/rest/',
  headers: {},
  options: {
    timeout: 10
  }
}

const jira = new JIRA(clientOptions)
```

This enables you to use `jira` with Jira Cloud.

#### Authentication

Basic
```js
jira.authenticate({
  type: 'basic',
  username: 'username',
  password: 'password'
})
```
OAuth
```js
jira.authenticate({
  type: 'token',
  token: 'access token'
})
```
