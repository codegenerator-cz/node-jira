const _ = require('lodash')
const path = require('path')
const Mustache = require('mustache')
const { readFileSync, writeFileSync } = require('fs')

const { pascalCase, getBadSchemaKeys } = require('./helpers.js')

const ROUTES = require('../src/routes/routes.json')

const typesMap = Object.assign(
  { integer: 'number' },
  {
    ComAtlassianJiraRestV2IssueIssueTypeResourceCreateIssueTypeAvatarPostBody:
      'any',
    ComAtlassianJiraRestV2IssueCommentPropertyResourceSetCommentPropertyPutBody:
      'any'
  },
  getBadSchemaKeys().reduce(
    (typesMap, key) => ({ ...typesMap, [key]: 'any' }),
    {}
  )
)

const parameterize = (paramName, param, consumes) => {
  if (param === null) {
    return {}
  }

  let enums = param.enum ? param.enum.map(JSON.stringify).join('|') : null
  //let enums = ""
  let type = typesMap[param.type] || param.type

  if (type === 'array') {
    type = `${param.itemsType || 'string'}[]`
  }

  let schema = false
  if (type === 'any' && param.schema) {
    schema = true
    type = param.schema
  }

  return {
    name: paramName,
    required: param.required,
    schema,
    type: enums || type
  }
}

const generateTypes = (languageName, templateFile, outputFile, typesBlob) => {
  let typesPath = path.resolve('src', outputFile)
  let templatePath = path.resolve('scripts/templates', templateFile)

  let template = readFileSync(templatePath, 'utf8')

  let namespaces = Object.keys(ROUTES).reduce((namespaces, namespaceName) => {
    let apis = _.toPairs(ROUTES[namespaceName]).reduce(
      (apis, [apiName, api]) => {
        let namespacedParamsName = pascalCase(`${namespaceName}-${apiName}`)

        if (api.alias) {
          let [namespaceAlias, apiAlias] = api.alias.split('.')
          api = ROUTES[namespaceAlias][apiAlias]
        }

        let ownParams = _.toPairs(api.params).reduce(
          (params, [paramName, param]) =>
            params.concat(parameterize(paramName, param, api.consumes)),
          []
        )

        if (api.bodyType) {
          ownParams.push({
            name: 'body',
            required: true,
            type: `JIRA.Schema.${pascalCase(
              typesMap[api.bodyType] || api.bodyType
            )}`
          })
        }

        let hasParams = ownParams.length > 0

        let paramsType = hasParams ? namespacedParamsName : pascalCase('Empty')

        let responseType = typesMap[api.responseType] || api.responseType || 'Any'
        
        if (responseType.slice(-2) === "[]") {
          responseType = pascalCase(responseType) + '[]'
        } else {
          responseType = pascalCase(responseType);
        }
        

        return apis.concat({
          name: _.camelCase(apiName),
          paramsType,
          params: hasParams && { items: ownParams },
          exclude: !hasParams,
          responseType
        })},
        []
      )

      return namespaces.concat({
        namespace: namespaceName,
        apis
      })
    }, [])

  let typesContent = Mustache.render(template, {
    namespaces,
    typesBlob
  })

  writeFileSync(typesPath, typesContent, 'utf8')
}

module.exports = generateTypes
