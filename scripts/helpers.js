const upperFirst = require('lodash/upperFirst')
const camelCase = require('lodash/camelCase')
const _ = require('lodash')

const pascalCase = string => upperFirst(camelCase(string))

const getBadSchemaKeys = () => {
  /**
   * IssueBean -> OpsbarBean -> LinkGroupBean -> LinkGroupBean
   * LinkGroupBean -> LinkGroupBean
   * NotificationEventBean -> NotificationEventBean
   * NotificationSchemeBean -> NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * OpsbarBean -> LinkGroupBean -> LinkGroupBean
   * PageBeanNotificationSchemeBean -> NotificationSchemeBean -> NotificationSchemeEventBean -> NotificationEventBean -> NotificationEventBean
   * SearchResultsBean -> IssueBean -> OpsbarBean -> LinkGroupBean -> LinkGroupBean
   */

  let badSchemaKeys = [
    //'IssueBean',
    //'LinkGroupBean',
    //'NotificationEventBean',
    //'NotificationSchemeBean',
    //'NotificationSchemeEventBean',
    //'OpsbarBean',
    //'PageBeanNotificationSchemeBean',
    //'SearchResultsBean'
  ]

  return badSchemaKeys
}

const extractMethodNamesForScopeFromMethodList = (methodsList, scope) => {
  return _.chain(methodsList)
    .values()
    .flatMap(_.values)
    .flatMap(o => _.pick(o, scope))
    .reject(_.isEmpty)
    .flatMap(_.values)
    .uniq()
    .compact()
    .value()
}

const extractScopesFromMethodsList = methodsList => {
  return _.chain(methodsList)
    .values()
    .flatMap(_.values)
    .flatMap(_.keys)
    .uniq()
    .value()
}

const getDuplicates = array => {
  return _.chain(array)
    .filter((value, index, iteratee) => _.includes(iteratee, value, index + 1))
    .uniq()
    .value()
}

const extractNamespaceFromURL = (url => {
    let parts = /^\/api\/2\/(\w+)\/?/.exec(url);
    if (parts && parts.length > 1) {
      return parts[1];
    }
    // hack for dumb jira v1 api
    // /auth/1/session
    else {
      let parts = /^\/auth\/1\/(\w+)\/?/.exec(url);
      if (parts && parts.length > 1) {
        return parts[1];
      }
    }
    return "";
});

module.exports = {
  getDuplicates,
  camelCase,
  pascalCase,
  getBadSchemaKeys,
  extractMethodNamesForScopeFromMethodList,
  extractNamespaceFromURL,
  extractScopesFromMethodsList
}
