const path = require('path')
const fetch = require('node-fetch')
const { writeFileSync } = require('fs')
const deepsort = require('deep-sort-object')

const specPath = path.resolve('specification')
const srcPath = path.resolve('src')

const writeSpecPartialJSON = (filename, content) => {
  writeFileSync(
    path.resolve(specPath, `${filename}.json`),
    `${JSON.stringify(deepsort(content), null, 2)}\n`
  )
}

const API_SPECIFICATION =
  'https://developer.atlassian.com/cloud/jira/platform/swagger.v3.json'

fetch(API_SPECIFICATION)
  .then(response => response.json())
  .then(({ paths, ...apiSpec }) => {
    writeSpecPartialJSON('paths', paths)
    return apiSpec
  })
  .then(({ components, ...apiSpec }) => {
    writeSpecPartialJSON('requestBodies', components.requestBodies)
    writeSpecPartialJSON('schemas', components.schemas)
    writeSpecPartialJSON('securitySchemes', components.securitySchemes)
    return apiSpec
  })
  .then(
    ({
      'x-atlassian-narrative': xAtlassianNarrative,
      externalDocs,
      openapi,
      servers,
      ...apiSpec
    }) => {
      writeSpecPartialJSON('others', apiSpec)
    }
  )
  .catch(err => {
    console.error(err)
  })
