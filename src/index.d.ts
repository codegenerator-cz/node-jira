/**
 * This declaration file requires TypeScript 2.1 or above.
 */

declare namespace JIRA {
  export type Any = any;
  export type AnyObject = object;

  export type Headers = {
    [header: string]: any
  };

  export interface Options {
    baseUrl?: string;
    headers?: Headers;
    options?: AnyObject;
  }

  interface AuthBasic {
    type: "basic";
    username: string;
    password: string;
  }

  interface AuthAppPassword {
    type: "apppassword";
    username: string;
    password: string;
  }

  interface AuthToken {
    type: "token";
    token: string;
  }

  export type Auth =
    | AuthBasic
    | AuthAppPassword
    | AuthToken;

  export type ResponseHeaders = {
    [header: string]: any;
  };

  export type ResponseError =
    | JIRA.Schema.ErrorCollection
    | JIRA.Schema.Any;

  export interface Callback<T> {
    (error: ResponseError, response: T): void;
  }

  export interface Response<T> {
    data: T;
    headers: ResponseHeaders;
  }

  namespace Schema {
    export type Any = JIRA.Any;
    export type AnyObject = JIRA.AnyObject;

      export interface ActorInputBean {
      group?: string[];
      user?: string[];
      [k: string]: any;
    }
    export interface ActorsMap {
      group?: string[];
      user?: string[];
      [k: string]: any;
    }
    export interface AddFieldBean {
      fieldId: string;
      [k: string]: any;
    }
    export interface AddGroupBean {
      name: string;
      [k: string]: any;
    }
    export interface ApplicationPropertyBean {
      allowedValues?: string[];
      defaultValue?: string;
      desc?: string;
      example?: string;
      id?: string;
      key?: string;
      name?: string;
      type?: string;
      value?: string;
      [k: string]: any;
    }
    export interface ApplicationRoleBean {
      defaultGroups?: string[];
      defined?: boolean;
      groups?: string[];
      hasUnlimitedSeats?: boolean;
      key?: string;
      name?: string;
      numberOfSeats?: number;
      platform?: boolean;
      remainingSeats?: number;
      selectedByDefault?: boolean;
      userCount?: number;
      userCountDescription?: string;
      [k: string]: any;
    }
    export interface AttachmentArchiveImpl {
      entries?: AttachmentArchiveEntry[];
      totalEntryCount?: number;
      [k: string]: any;
    }
    export interface AttachmentArchiveEntry {
      abbreviatedName?: string;
      entryIndex?: number;
      mediaType?: string;
      name?: string;
      size?: number;
      [k: string]: any;
    }
    export interface AttachmentBean {
      author?: User;
      content?: string;
      created?: string;
      filename?: string;
      id?: number;
      mimeType?: string;
      properties?: {
        [k: string]: any;
      };
      self?: string;
      size?: number;
      thumbnail?: string;
      [k: string]: any;
    }
    export interface User {
      accountId?: string;
      active?: boolean;
      applicationRoles?: SimpleListWrapperApplicationRoleBean;
      avatarUrls?: {
        [k: string]: string;
      };
      displayName?: string;
      emailAddress?: string;
      expand?: string;
      groups?: SimpleListWrapperGroupJsonBean;
      key?: string;
      locale?: string;
      name?: string;
      self?: string;
      timeZone?: string;
      [k: string]: any;
    }
    export interface SimpleListWrapperApplicationRoleBean {
      callback?: ListWrapperCallbackApplicationRoleBean;
      items?: {
        defaultGroups?: string[];
        defined?: boolean;
        groups?: string[];
        hasUnlimitedSeats?: boolean;
        key?: string;
        name?: string;
        numberOfSeats?: number;
        platform?: boolean;
        remainingSeats?: number;
        selectedByDefault?: boolean;
        userCount?: number;
        userCountDescription?: string;
        [k: string]: any;
      }[];
      "max-results"?: number;
      pagingCallback?: ListWrapperCallbackApplicationRoleBean;
      size?: number;
      [k: string]: any;
    }
    export interface ListWrapperCallbackApplicationRoleBean {
      [k: string]: any;
    }
    export interface SimpleListWrapperGroupJsonBean {
      callback?: ListWrapperCallbackGroupJsonBean;
      items?: {
        name?: string;
        self?: string;
        [k: string]: any;
      }[];
      "max-results"?: number;
      pagingCallback?: ListWrapperCallbackGroupJsonBean;
      size?: number;
      [k: string]: any;
    }
    export interface ListWrapperCallbackGroupJsonBean {
      [k: string]: any;
    }
    export interface AttachmentJsonBean {
      author?: UserJsonBean;
      content?: string;
      created?: string;
      filename?: string;
      id?: string;
      mimeType?: string;
      self?: string;
      size?: number;
      thumbnail?: string;
      [k: string]: any;
    }
    export interface UserJsonBean {
      accountId?: string;
      active?: boolean;
      avatarUrls?: {
        [k: string]: string;
      };
      displayName?: string;
      emailAddress?: string;
      key?: string;
      name?: string;
      self?: string;
      timeZone?: string;
      [k: string]: any;
    }
    export interface AttachmentMetaBean {
      enabled?: boolean;
      uploadLimit?: number;
      [k: string]: any;
    }
    export interface AuditingResponseBean {
      limit?: number;
      offset?: number;
      records?: AuditRecordBean[];
      total?: number;
      [k: string]: any;
    }
    export interface AuditRecordBean {
      associatedItems?: AssociatedItemBean[];
      authorKey?: string;
      category?: string;
      changedValues?: ChangedValueBean[];
      created?: string;
      description?: string;
      eventSource?: string;
      id?: number;
      objectItem?: AssociatedItemBean;
      remoteAddress?: string;
      summary?: string;
      [k: string]: any;
    }
    export interface AssociatedItemBean {
      id?: string;
      name?: string;
      parentId?: string;
      parentName?: string;
      typeName?: string;
      [k: string]: any;
    }
    export interface ChangedValueBean {
      changedFrom?: string;
      changedTo?: string;
      fieldName?: string;
      [k: string]: any;
    }
    export interface AuthParams {
      password: string;
      username: string;
      [k: string]: any;
    }
    export interface AuthSuccess {
      loginInfo?: LoginInfo;
      session?: SessionInfo;
      [k: string]: any;
    }
    export interface LoginInfo {
      failedLoginCount?: number;
      lastFailedLoginTime?: string;
      loginCount?: number;
      previousLoginTime?: string;
      [k: string]: any;
    }
    export interface SessionInfo {
      name?: string;
      value?: string;
      [k: string]: any;
    }
    export interface AutoCompleteResponseBean {
      jqlReservedWords?: string[];
      visibleFieldNames?: VisibleFieldNameBean[];
      visibleFunctionNames?: VisibleFunctionNameBean[];
      [k: string]: any;
    }
    export interface VisibleFieldNameBean {
      auto?: string;
      cfid?: string;
      displayName?: string;
      operators?: string[];
      orderable?: string;
      searchable?: string;
      types?: string[];
      value?: string;
      [k: string]: any;
    }
    export interface VisibleFunctionNameBean {
      displayName?: string;
      isList?: string;
      types?: string[];
      value?: string;
      [k: string]: any;
    }
    export interface AutoCompleteResultWrapper {
      results?: AutoCompleteSuggestionBean[];
      [k: string]: any;
    }
    export interface AutoCompleteSuggestionBean {
      displayName?: string;
      value?: string;
      [k: string]: any;
    }
    export interface AvatarBean {
      fileName?: string;
      id?: string;
      isDeletable?: boolean;
      isSelected?: boolean;
      isSystemAvatar?: boolean;
      owner?: string;
      urls?: {
        [k: string]: string;
      };
      [k: string]: any;
    }
    export interface AvatarsBean {
      custom?: AvatarBean[];
      system?: AvatarBean[];
      [k: string]: any;
    }
    export interface HistoryMetadata {
      activityDescription?: string;
      actor?: HistoryMetadataParticipant;
      cause?: HistoryMetadataParticipant;
      description?: string;
      emailDescription?: string;
      extraData?: {
        [k: string]: string;
      };
      generator?: HistoryMetadataParticipant;
      type?: string;
      [k: string]: any;
    }
    export interface HistoryMetadataParticipant {
      avatarUrl?: string;
      displayName?: string;
      id?: string;
      type?: string;
      url?: string;
      [k: string]: any;
    }
    export interface ChangeItemBean {
      field?: string;
      fieldId?: string;
      fieldtype?: string;
      from?: string;
      fromString?: string;
      to?: string;
      toString?: string;
      [k: string]: any;
    }
    export interface ColumnItem {
      label?: string;
      value?: string;
      [k: string]: any;
    }
    export interface CommentJsonBean {
      author?: UserJsonBean;
      body?: string;
      created?: string;
      id?: string;
      jsdPublic?: boolean;
      properties?: EntityPropertyBean[];
      renderedBody?: string;
      self?: string;
      updateAuthor?: UserJsonBean;
      updated?: string;
      visibility?: VisibilityJsonBean;
      [k: string]: any;
    }
    export interface EntityPropertyBean {
      key?: string;
      value?: any;
      [k: string]: any;
    }
    export interface VisibilityJsonBean {
      type?: "group" | "role";
      value?: string;
      [k: string]: any;
    }
    export interface ComponentBean {
      assignee?: User;
      assigneeType?: "PROJECT_DEFAULT" | "COMPONENT_LEAD" | "PROJECT_LEAD" | "UNASSIGNED";
      description?: string;
      id?: string;
      isAssigneeTypeValid?: boolean;
      lead?: User;
      leadAccountId?: string;
      leadUserName?: string;
      name?: string;
      project?: string;
      projectId?: number;
      realAssignee?: User;
      realAssigneeType?: "PROJECT_DEFAULT" | "COMPONENT_LEAD" | "PROJECT_LEAD" | "UNASSIGNED";
      self?: string;
      [k: string]: any;
    }
    export interface ComponentIssueCountsBean {
      issueCount?: number;
      self?: string;
      [k: string]: any;
    }
    export interface ConfigurationBean {
      attachmentsEnabled?: boolean;
      issueLinkingEnabled?: boolean;
      subTasksEnabled?: boolean;
      timeTrackingConfiguration?: TimeTrackingConfigurationBean;
      timeTrackingEnabled?: boolean;
      unassignedIssuesAllowed?: boolean;
      votingEnabled?: boolean;
      watchingEnabled?: boolean;
      [k: string]: any;
    }
    export interface TimeTrackingConfigurationBean {
      defaultUnit: "minute" | "hour" | "day" | "week";
      timeFormat: "pretty" | "days" | "hours";
      workingDaysPerWeek: number;
      workingHoursPerDay: number;
      [k: string]: any;
    }
    export interface CreateMetaBean {
      expand?: string;
      projects?: CreateMetaProjectBean[];
      [k: string]: any;
    }
    export interface CreateMetaProjectBean {
      avatarUrls?: {
        [k: string]: string;
      };
      expand?: string;
      id?: string;
      issuetypes?: CreateMetaIssueTypeBean[];
      key?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface CreateMetaIssueTypeBean {
      avatarId?: number;
      description?: string;
      expand?: string;
      fields?: {
        [k: string]: FieldMetaBean;
      };
      iconUrl?: string;
      id?: string;
      name?: string;
      scope?: EntityScopeJsonBean;
      self?: string;
      subtask?: boolean;
      [k: string]: any;
    }
    export interface FieldMetaBean {
      allowedValues?: any[];
      autoCompleteUrl?: string;
      defaultValue?: any;
      hasDefaultValue?: boolean;
      key: string;
      name: string;
      operations: string[];
      required: boolean;
      schema: JsonTypeBean;
      [k: string]: any;
    }
    export interface JsonTypeBean {
      custom?: string;
      customId?: number;
      items?: string;
      system?: string;
      type: string;
      [k: string]: any;
    }
    export interface EntityScopeJsonBean {
      project?: ProjectJsonBean;
      type?: "PROJECT" | "TEMPLATE";
      [k: string]: any;
    }
    export interface ProjectJsonBean {
      avatarUrls?: {
        [k: string]: string;
      };
      id?: string;
      key?: string;
      name?: string;
      projectCategory?: ProjectCategoryJsonBean;
      projectTypeKey?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectCategoryJsonBean {
      description?: string;
      id?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface CreateUpdateRoleRequestBean {
      description?: string;
      name?: string;
      [k: string]: any;
    }
    export interface CurrentUser {
      loginInfo?: LoginInfo;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface CustomFieldDefinitionJsonBean {
      description?: string;
      name: string;
      searcherKey:
        | "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselectsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:daterange"
        | "com.atlassian.jira.plugin.system.customfieldtypes:datetimerange"
        | "com.atlassian.jira.plugin.system.customfieldtypes:exactnumber"
        | "com.atlassian.jira.plugin.system.customfieldtypes:exacttextsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:grouppickersearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:labelsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multiselectsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:numberrange"
        | "com.atlassian.jira.plugin.system.customfieldtypes:projectsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher"
        | "com.atlassian.jira.plugin.system.customfieldtypes:versionsearcher";
      type:
        | "com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect"
        | "com.atlassian.jira.plugin.system.customfieldtypes:datepicker"
        | "com.atlassian.jira.plugin.system.customfieldtypes:datetime"
        | "com.atlassian.jira.plugin.system.customfieldtypes:float"
        | "com.atlassian.jira.plugin.system.customfieldtypes:grouppicker"
        | "com.atlassian.jira.plugin.system.customfieldtypes:importid"
        | "com.atlassian.jira.plugin.system.customfieldtypes:labels"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multiselect"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker"
        | "com.atlassian.jira.plugin.system.customfieldtypes:multiversion"
        | "com.atlassian.jira.plugin.system.customfieldtypes:project"
        | "com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons"
        | "com.atlassian.jira.plugin.system.customfieldtypes:readonlyfield"
        | "com.atlassian.jira.plugin.system.customfieldtypes:select"
        | "com.atlassian.jira.plugin.system.customfieldtypes:textarea"
        | "com.atlassian.jira.plugin.system.customfieldtypes:textfield"
        | "com.atlassian.jira.plugin.system.customfieldtypes:url"
        | "com.atlassian.jira.plugin.system.customfieldtypes:userpicker"
        | "com.atlassian.jira.plugin.system.customfieldtypes:version";
      [k: string]: any;
    }
    export interface CustomFieldOptionBean {
      self?: string;
      value?: string;
      [k: string]: any;
    }
    export interface DashboardBean {
      description?: string;
      id?: string;
      isFavourite?: boolean;
      name?: string;
      owner?: UserBean;
      popularity?: number;
      rank?: number;
      self?: string;
      sharePermissions?: SharePermissionBean[];
      view?: string;
      [k: string]: any;
    }
    export interface UserBean {
      accountId?: string;
      active?: boolean;
      avatarUrls?: UserBeanAvatarUrls;
      displayName?: string;
      key?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface UserBeanAvatarUrls {
      "16x16"?: string;
      "24x24"?: string;
      "32x32"?: string;
      "48x48"?: string;
      [k: string]: any;
    }
    export interface SharePermissionBean {
      group?: GroupBean;
      id?: number;
      project?: ProjectIdentityBean;
      role?: ProjectRoleBean;
      type?: string;
      [k: string]: any;
    }
    export interface GroupBean {
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectIdentityBean {
      id?: number;
      key?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectRoleBean {
      actors?: RoleActorBean[];
      description?: string;
      id?: number;
      name?: string;
      scope?: EntityScopeJsonBean;
      self?: string;
      [k: string]: any;
    }
    export interface RoleActorBean {
      actorGroup?: ProjectRoleGroupBean;
      actorUser?: ProjectRoleUserBean;
      avatarUrl?: string;
      displayName?: string;
      id?: number;
      name?: string;
      type?: string;
      user?: string;
      [k: string]: any;
    }
    export interface ProjectRoleGroupBean {
      displayName?: string;
      name?: string;
      [k: string]: any;
    }
    export interface ProjectRoleUserBean {
      accountId?: string;
      [k: string]: any;
    }
    export interface DashboardsBean {
      [k: string]: any;
    }
    export interface DefaultBean {
      updateDraftIfNeeded?: boolean;
      workflow: string;
      [k: string]: any;
    }
    export interface DefaultShareScopeBean {
      scope: "GLOBAL" | "AUTHENTICATED" | "PRIVATE";
      [k: string]: any;
    }
    export interface DeleteAndReplaceVersionBean {
      customFieldReplacementList?: CustomFieldReplacement[];
      moveAffectedIssuesTo?: number;
      moveFixIssuesTo?: number;
      [k: string]: any;
    }
    export interface CustomFieldReplacement {
      customFieldId?: number;
      moveTo?: number;
      [k: string]: any;
    }
    export interface EditMetaBean {
      fields?: {
        [k: string]: FieldMetaBean;
      };
      [k: string]: any;
    }
    export interface EntityPropertiesKeysBean {
      keys?: EntityPropertyKeyBean[];
      [k: string]: any;
    }
    export interface EntityPropertyKeyBean {
      key?: string;
      self?: string;
      [k: string]: any;
    }
    export interface EntityPropertyBulkDeleteFilterBean {
      currentValue?: any;
      entityIds?: number[];
      [k: string]: any;
    }
    export interface EntityPropertyBulkUpdateRequestBean {
      filter?: EntityPropertyBulkSetFilterBean;
      value?: any;
      [k: string]: any;
    }
    export interface EntityPropertyBulkSetFilterBean {
      currentValue?: any;
      entityIds?: number[];
      hasProperty?: boolean;
      [k: string]: any;
    }
    export interface ErrorCollection {
      errorMessages?: string[];
      errors?: {
        [k: string]: string;
      };
      status?: number;
      [k: string]: any;
    }
    export interface FieldBean {
      clauseNames?: string[];
      custom?: boolean;
      id?: string;
      key?: string;
      name?: string;
      navigable?: boolean;
      orderable?: boolean;
      schema?: JsonTypeBean;
      searchable?: boolean;
      [k: string]: any;
    }
    export interface IssueTypeJsonBean {
      avatarId?: number;
      description?: string;
      iconUrl?: string;
      id?: string;
      name?: string;
      scope?: EntityScopeJsonBean;
      self?: string;
      subtask?: boolean;
      [k: string]: any;
    }
    export interface PriorityJsonBean {
      description?: string;
      iconUrl?: string;
      id?: string;
      name?: string;
      self?: string;
      statusColor?: string;
      [k: string]: any;
    }
    export interface StatusJsonBean {
      description?: string;
      iconUrl?: string;
      id?: string;
      name?: string;
      self?: string;
      statusCategory?: StatusCategoryJsonBean;
      [k: string]: any;
    }
    export interface StatusCategoryJsonBean {
      colorName?: string;
      id?: number;
      key?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface FilterBean {
      description?: string;
      favourite?: boolean;
      favouritedCount?: number;
      id?: string;
      jql?: string;
      name: string;
      owner?: User;
      searchUrl?: string;
      self?: string;
      sharedUsers?: UserBeanListWrapper;
      sharePermissions?: FilterPermissionBean[];
      subscriptions?: FilterSubscriptionBeanListWrapper;
      viewUrl?: string;
      [k: string]: any;
    }
    export interface UserBeanListWrapper {
      backingListSize?: number;
      callback?: ListWrapperCallbackUser;
      "end-index"?: number;
      items?: {
        accountId?: string;
        active?: boolean;
        applicationRoles?: SimpleListWrapperApplicationRoleBean;
        avatarUrls?: {
          [k: string]: string;
        };
        displayName?: string;
        emailAddress?: string;
        expand?: string;
        groups?: SimpleListWrapperGroupJsonBean;
        key?: string;
        locale?: string;
        name?: string;
        self?: string;
        timeZone?: string;
        [k: string]: any;
      }[];
      "max-results"?: number;
      pagingCallback?: ListWrapperCallbackUser;
      size?: number;
      "start-index"?: number;
      [k: string]: any;
    }
    export interface ListWrapperCallbackUser {
      [k: string]: any;
    }
    export interface FilterPermissionBean {
      group?: GroupJsonBean;
      id?: number;
      project?: ProjectBean;
      role?: ProjectRoleBean;
      type: "group" | "project" | "global" | "loggedin" | "authenticated" | "project-unknown";
      [k: string]: any;
    }
    export interface GroupJsonBean {
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectBean {
      assigneeType?: "PROJECT_LEAD" | "UNASSIGNED";
      avatarUrls?: {
        [k: string]: string;
      };
      components?: ComponentBean[];
      description?: string;
      expand?: string;
      favourite?: boolean;
      id?: string;
      isPrivate?: boolean;
      issueTypes?: IssueTypeJsonBean[];
      key?: string;
      lead?: User;
      name?: string;
      projectTypeKey?: string;
      roles?: {
        [k: string]: string;
      };
      self?: string;
      simplified?: boolean;
      style?: "classic" | "next-gen";
      url?: string;
      versions?: VersionBean[];
      [k: string]: any;
    }
    export interface VersionBean {
      archived?: boolean;
      description?: string;
      expand?: string;
      id?: string;
      issuesStatusForFixVersion?: VersionIssuesStatusBean;
      moveUnfixedIssuesTo?: string;
      name?: string;
      operations?: SimpleLinkBean[];
      overdue?: boolean;
      project?: string;
      projectId?: number;
      released?: boolean;
      releaseDate?: string;
      remotelinks?: RemoteEntityLinkJsonBean[];
      self?: string;
      startDate?: string;
      userReleaseDate?: string;
      userStartDate?: string;
      [k: string]: any;
    }
    export interface VersionIssuesStatusBean {
      done?: number;
      inProgress?: number;
      toDo?: number;
      unmapped?: number;
      [k: string]: any;
    }
    export interface SimpleLinkBean {
      href?: string;
      iconClass?: string;
      id?: string;
      label?: string;
      styleClass?: string;
      title?: string;
      weight?: number;
      [k: string]: any;
    }
    export interface RemoteEntityLinkJsonBean {
      link?: any;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface FilterSubscriptionBeanListWrapper {
      backingListSize?: number;
      callback?: ListWrapperCallbackFilterSubscriptionBean;
      "end-index"?: number;
      items?: {
        group?: GroupJsonBean;
        id?: number;
        user?: User;
        [k: string]: any;
      }[];
      "max-results"?: number;
      pagingCallback?: ListWrapperCallbackFilterSubscriptionBean;
      size?: number;
      "start-index"?: number;
      [k: string]: any;
    }
    export interface ListWrapperCallbackFilterSubscriptionBean {
      [k: string]: any;
    }
    export interface FilterSubscriptionBean {
      group?: GroupJsonBean;
      id?: number;
      user?: User;
      [k: string]: any;
    }
    export interface Group {
      name?: string;
      self?: string;
      users?: PagedListWrapperUserJsonBeanApplicationUser;
      [k: string]: any;
    }
    export interface PagedListWrapperUserJsonBeanApplicationUser {
      backingListSize?: number;
      callback?: ListWrapperCallbackUserJsonBean;
      "end-index"?: number;
      items?: {
        accountId?: string;
        active?: boolean;
        avatarUrls?: {
          [k: string]: string;
        };
        displayName?: string;
        emailAddress?: string;
        key?: string;
        name?: string;
        self?: string;
        timeZone?: string;
        [k: string]: any;
      }[];
      "max-results"?: number;
      pagingCallback?: ListWrapperCallbackUserJsonBean;
      size?: number;
      "start-index"?: number;
      [k: string]: any;
    }
    export interface ListWrapperCallbackUserJsonBean {
      [k: string]: any;
    }
    export interface GroupLabelBean {
      text?: string;
      title?: string;
      type?: "ADMIN" | "SINGLE" | "MULTIPLE";
      [k: string]: any;
    }
    export interface GroupSuggestionsBean {
      groups?: GroupSuggestionBean[];
      header?: string;
      total?: number;
      [k: string]: any;
    }
    export interface GroupSuggestionBean {
      html?: string;
      labels?: GroupLabelBean[];
      name?: string;
      [k: string]: any;
    }
    export interface HumanReadableArchive {
      entries?: HumanReadableArchiveEntry[];
      id?: number;
      mediaType?: string;
      name?: string;
      totalEntryCount?: number;
      [k: string]: any;
    }
    export interface HumanReadableArchiveEntry {
      index?: number;
      label?: string;
      mediaType?: string;
      path?: string;
      size?: string;
      [k: string]: any;
    }
    export interface IdBean {
      id: number;
      [k: string]: any;
    }
    export interface IssueBean {
      changelog?: PaginatedChangelogsJsonBean;
      editmeta?: EditMetaBean;
      editMeta?: EditMetaBean;
      expand?: string;
      fields?: {
        [k: string]: any;
      };
      id?: string;
      key?: string;
      names?: {
        [k: string]: string;
      };
      operations?: OpsbarBean;
      properties?: {
        [k: string]: any;
      };
      renderedFields?: {
        [k: string]: any;
      };
      schema?: {
        [k: string]: JsonTypeBean;
      };
      self?: string;
      transitions?: TransitionBean[];
      versionedRepresentations?: {
        [k: string]: {
          [k: string]: any;
        };
      };
      [k: string]: any;
    }
    export interface PaginatedChangelogsJsonBean {
      histories?: ChangeHistoryBean[];
      maxResults?: number;
      startAt?: number;
      total?: number;
      [k: string]: any;
    }
    export interface ChangeHistoryBean {
      author?: UserJsonBean;
      created?: string;
      historyMetadata?: HistoryMetadata;
      id?: string;
      items?: ChangeItemBean[];
      [k: string]: any;
    }
    export interface OpsbarBean {
      linkGroups?: LinkGroupBean[];
      [k: string]: any;
    }
    export interface LinkGroupBean {
      groups?: LinkGroupBean[];
      header?: SimpleLinkBean;
      id?: string;
      links?: SimpleLinkBean[];
      styleClass?: string;
      weight?: number;
      [k: string]: any;
    }
    export interface TransitionBean {
      expand?: string;
      fields?: {
        [k: string]: FieldMetaBean;
      };
      hasScreen?: boolean;
      id?: string;
      isConditional?: boolean;
      isGlobal?: boolean;
      isInitial?: boolean;
      name?: string;
      to?: StatusJsonBean;
      [k: string]: any;
    }
    export interface IssueCommentListRequestBean {
      ids?: number[];
      [k: string]: any;
    }
    export interface IssueCreateResponse {
      id: string;
      key: string;
      self: string;
      transition?: NestedResponse;
      [k: string]: any;
    }
    export interface NestedResponse {
      errorCollection?: ErrorCollection;
      status?: number;
      [k: string]: any;
    }
    export interface IssueFieldOptionBean {
      config?: IssueFieldOptionConfigBean;
      id: number;
      properties?: {
        [k: string]: any;
      };
      value: string;
      [k: string]: any;
    }
    export interface IssueFieldOptionConfigBean {
      attributes?: ("notSelectable" | "defaultValue")[];
      scope?: IssueFieldOptionScopeBean;
      [k: string]: any;
    }
    export interface IssueFieldOptionScopeBean {
      global?: GlobalScopeBean;
      projects?: number[];
      projects2?: ProjectScopeBean[];
      [k: string]: any;
    }
    export interface GlobalScopeBean {
      attributes?: ("notSelectable" | "defaultValue")[];
      [k: string]: any;
    }
    export interface ProjectScopeBean {
      attributes?: ("notSelectable" | "defaultValue")[];
      id?: number;
      [k: string]: any;
    }
    export interface IssueFieldOptionCreateBean {
      config?: IssueFieldOptionConfigBean;
      properties?: {
        [k: string]: any;
      };
      value: string;
      [k: string]: any;
    }
    export interface IssueLinkJsonBean {
      id?: string;
      inwardIssue?: IssueRefJsonBean;
      outwardIssue?: IssueRefJsonBean;
      self?: string;
      type?: IssueLinkTypeJsonBean;
      [k: string]: any;
    }
    export interface IssueRefJsonBean {
      fields?: Fields;
      id?: string;
      key?: string;
      self?: string;
      [k: string]: any;
    }
    export interface Fields {
      issuetype?: IssueTypeJsonBean;
      priority?: PriorityJsonBean;
      status?: StatusJsonBean;
      summary?: string;
      [k: string]: any;
    }
    export interface IssueLinkTypeJsonBean {
      id?: string;
      inward?: string;
      name?: string;
      outward?: string;
      self?: string;
      [k: string]: any;
    }
    export interface IssueLinkTypesBean {
      issueLinkTypes?: {
        id?: string;
        inward?: string;
        name?: string;
        outward?: string;
        self?: string;
        [k: string]: any;
      }[];
      [k: string]: any;
    }
    export interface IssuePickerResult {
      sections?: IssueSection[];
      [k: string]: any;
    }
    export interface IssueSection {
      id?: string;
      issues?: IssuePickerIssue[];
      label?: string;
      msg?: string;
      sub?: string;
      [k: string]: any;
    }
    export interface IssuePickerIssue {
      img?: string;
      key?: string;
      keyHtml?: string;
      summary?: string;
      summaryText?: string;
      [k: string]: any;
    }
    export interface IssuesUpdateBean {
      issueUpdates?: IssueUpdateBean[];
      [k: string]: any;
    }
    export interface IssueUpdateBean {
      fields?: {
        [k: string]: any;
      };
      historyMetadata?: HistoryMetadata;
      properties?: EntityPropertyBean[];
      transition?: TransitionBean;
      update?: {
        [k: string]: FieldUpdateOperationBean[];
      };
      [k: string]: any;
    }
    export interface FieldUpdateOperationBean {
      add?: {
        [k: string]: any;
      };
      edit?: {
        [k: string]: any;
      };
      remove?: {
        [k: string]: any;
      };
      set?: {
        [k: string]: any;
      };
      [k: string]: any;
    }
    export interface IssueTypeCreateBean {
      description?: string;
      name: string;
      type?: "subtask" | "standard";
      [k: string]: any;
    }
    export interface IssueTypeMappingBean {
      issueType?: string;
      updateDraftIfNeeded?: boolean;
      workflow: string;
      [k: string]: any;
    }
    export interface IssueTypeUpdateBean {
      avatarId?: number;
      description?: string;
      name?: string;
      [k: string]: any;
    }
    export interface IssueTypeWithStatusJsonBean {
      id?: string;
      name?: string;
      self?: string;
      statuses?: StatusJsonBean[];
      subtask?: boolean;
      [k: string]: any;
    }
    export interface IdOrKeyBean {
      id?: number;
      key?: string;
      [k: string]: any;
    }
    export interface JiraExpressionEvalRequestBean {
      context?: JiraExpressionEvalContextBean;
      expression: string;
      [k: string]: any;
    }
    export interface JiraExpressionEvalContextBean {
      board?: number;
      issue?: IdOrKeyBean;
      project?: IdOrKeyBean;
      sprint?: number;
      [k: string]: any;
    }
    export interface JiraExpressionsComplexityBean {
      beans?: JiraExpressionsComplexityValueBean;
      expensiveOperations?: JiraExpressionsComplexityValueBean;
      primitiveValues?: JiraExpressionsComplexityValueBean;
      steps?: JiraExpressionsComplexityValueBean;
      [k: string]: any;
    }
    export interface JiraExpressionsComplexityValueBean {
      limit?: number;
      value?: number;
      [k: string]: any;
    }
    export interface JiraExpressionResultBean {
      meta?: JiraExpressionEvaluationMetaDataBean;
      value: any;
      [k: string]: any;
    }
    export interface JiraExpressionEvaluationMetaDataBean {
      complexity?: JiraExpressionsComplexityBean;
      [k: string]: any;
    }
    export interface JqlPersonalDataMigrationRequest {
      queryStrings?: string[];
      [k: string]: any;
    }
    export interface JqlPersonalDataMigrationResponse {
      queryStrings?: string[];
      [k: string]: any;
    }
    export interface LinkIssueRequestJsonBean {
      comment?: CommentJsonBean;
      inwardIssue: IssueRefJsonBean;
      outwardIssue: IssueRefJsonBean;
      type: IssueLinkTypeJsonBean;
      [k: string]: any;
    }
    export interface LocaleBean {
      locale?: string;
      [k: string]: any;
    }
    export interface MoveFieldBean {
      after?: string;
      position?: "Earlier" | "Later" | "First" | "Last";
      [k: string]: any;
    }
    export interface NotificationEventBean {
      description?: string;
      id?: number;
      name?: string;
      templateEvent?: NotificationEventBean;
      [k: string]: any;
    }
    export interface NotificationJsonBean {
      htmlBody?: string;
      restrict: RestrictJsonBean;
      subject?: string;
      textBody?: string;
      to: ToJsonBean;
      [k: string]: any;
    }
    export interface RestrictJsonBean {
      groups?: GroupJsonBean[];
      permissions?: PermissionJsonBean[];
      [k: string]: any;
    }
    export interface PermissionJsonBean {
      id?: string;
      key?: string;
      [k: string]: any;
    }
    export interface ToJsonBean {
      assignee?: boolean;
      groups?: GroupJsonBean[];
      reporter?: boolean;
      users?: UserJsonBean[];
      voters?: boolean;
      watchers?: boolean;
      [k: string]: any;
    }
    export interface NotificationSchemeBean {
      description?: string;
      expand?: string;
      id?: number;
      name?: string;
      notificationSchemeEvents?: NotificationSchemeEventBean[];
      self?: string;
      [k: string]: any;
    }
    export interface NotificationSchemeEventBean {
      event?: NotificationEventBean;
      notifications?: NotificationBean[];
      [k: string]: any;
    }
    export interface NotificationBean {
      emailAddress?: string;
      expand?: string;
      field?: FieldBean;
      group?: GroupJsonBean;
      id?: number;
      notificationType?:
        | "CurrentAssignee"
        | "Reporter"
        | "CurrentUser"
        | "ProjectLead"
        | "ComponentLead"
        | "User"
        | "Group"
        | "ProjectRole"
        | "EmailAddress"
        | "AllWatchers"
        | "UserCustomField"
        | "GroupCustomField";
      parameter?: string;
      projectRole?: ProjectRoleBean;
      user?: UserJsonBean;
      [k: string]: any;
    }
    export interface PageBeanChangeHistoryBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: ChangeHistoryBean[];
      [k: string]: any;
    }
    export interface PageBeanCommentJsonBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: CommentJsonBean[];
      [k: string]: any;
    }
    export interface PageBeanComponentWithIssueCountBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: ComponentWithIssueCountBean[];
      [k: string]: any;
    }
    export interface ComponentWithIssueCountBean {
      assignee?: User;
      assigneeType?: "PROJECT_DEFAULT" | "COMPONENT_LEAD" | "PROJECT_LEAD" | "UNASSIGNED";
      description?: string;
      id?: string;
      isAssigneeTypeValid?: boolean;
      issueCount?: number;
      lead?: User;
      leadUserName?: string;
      name?: string;
      project?: string;
      projectId?: number;
      realAssignee?: User;
      realAssigneeType?: "PROJECT_DEFAULT" | "COMPONENT_LEAD" | "PROJECT_LEAD" | "UNASSIGNED";
      self?: string;
      [k: string]: any;
    }
    export interface PageBeanFilterBean2 {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: FilterBean2[];
      [k: string]: any;
    }
    export interface FilterBean2 {
      description?: string;
      favourite?: boolean;
      favouritedCount?: number;
      id?: string;
      jql?: string;
      name?: string;
      owner?: User;
      searchUrl?: string;
      self?: string;
      sharePermissions?: FilterPermissionBean[];
      subscriptions?: FilterSubscriptionBean[];
      viewUrl?: string;
      [k: string]: any;
    }
    export interface PageBeanIssueFieldOptionBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: IssueFieldOptionBean[];
      [k: string]: any;
    }
    export interface PageBeanNotificationSchemeBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: NotificationSchemeBean[];
      [k: string]: any;
    }
    export interface PageBeanProjectBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: ProjectBean[];
      [k: string]: any;
    }
    export interface PageBeanScreenBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: ScreenBean[];
      [k: string]: any;
    }
    export interface ScreenBean {
      id?: number;
      name?: string;
      [k: string]: any;
    }
    export interface PageBeanUser {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: User[];
      [k: string]: any;
    }
    export interface PageBeanUserJsonBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: UserJsonBean[];
      [k: string]: any;
    }
    export interface PageBeanUserKeyBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: UserKeyBean[];
      [k: string]: any;
    }
    export interface UserKeyBean {
      accountId?: string;
      key?: string;
      [k: string]: any;
    }
    export interface PageBeanVersionBean {
      isLast?: boolean;
      maxResults?: number;
      nextPage?: string;
      self?: string;
      startAt?: number;
      total?: number;
      values?: VersionBean[];
      [k: string]: any;
    }
    export interface ListWrapperCallbackObject {
      [k: string]: any;
    }
    export interface PaginatedCommentsJsonBean {
      comments?: CommentJsonBean[];
      maxResults?: number;
      startAt?: number;
      total?: number;
      [k: string]: any;
    }
    export interface PermissionGrantBean {
      holder?: PermissionHolderBean;
      id?: number;
      permission?: string;
      self?: string;
      [k: string]: any;
    }
    export interface PermissionHolderBean {
      expand?: string;
      field?: FieldBean;
      group?: GroupJsonBean;
      parameter?: string;
      projectRole?: ProjectRoleBean;
      type?: string;
      user?: UserJsonBean;
      [k: string]: any;
    }
    export interface PermissionGrantsBean {
      expand?: string;
      permissions?: PermissionGrantBean[];
      [k: string]: any;
    }
    export interface PermissionSchemeBean {
      description?: string;
      expand?: string;
      id?: number;
      name: string;
      permissions?: PermissionGrantBean[];
      scope?: EntityScopeJsonBean;
      self?: string;
      [k: string]: any;
    }
    export interface PermissionSchemesBean {
      permissionSchemes?: PermissionSchemeBean[];
      [k: string]: any;
    }
    export interface PermissionsJsonBean {
      permissions?: {
        [k: string]: UserPermissionJsonBean;
      };
      [k: string]: any;
    }
    export interface UserPermissionJsonBean {
      deprecatedKey?: boolean;
      description?: string;
      havePermission?: boolean;
      id?: string;
      key?: string;
      name?: string;
      type?: "GLOBAL" | "PROJECT";
      [k: string]: any;
    }
    export interface PermissionsKeysBean {
      permissions: string[];
      [k: string]: any;
    }
    export interface PermittedProjectsBean {
      projects?: ProjectIdentifierBean[];
      [k: string]: any;
    }
    export interface ProjectIdentifierBean {
      id?: number;
      key?: string;
      [k: string]: any;
    }
    export interface ProjectAvatars {
      custom: AvatarBean[];
      system: AvatarBean[];
      [k: string]: any;
    }
    export interface ProjectCategoryBean {
      description?: string;
      id?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectIdentity {
      id?: number;
      key?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ProjectInputBean {
      assigneeType?: "PROJECT_LEAD" | "UNASSIGNED";
      avatarId?: number;
      categoryId?: number;
      description?: string;
      issueSecurityScheme?: number;
      key?: string;
      lead?: string;
      leadAccountId?: string;
      name?: string;
      notificationScheme?: number;
      permissionScheme?: number;
      projectTemplateKey?:
        | "com.pyxis.greenhopper.jira:gh-simplified-agility"
        | "com.pyxis.greenhopper.jira:gh-simplified-basic"
        | "com.pyxis.greenhopper.jira:gh-simplified-kanban"
        | "com.pyxis.greenhopper.jira:gh-simplified-scrum"
        | "com.atlassian.servicedesk:simplified-it-service-desk"
        | "com.atlassian.servicedesk:simplified-internal-service-desk"
        | "com.atlassian.servicedesk:simplified-external-service-desk"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-content-management"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-document-approval"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-lead-tracking"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-process-control"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-procurement"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-project-management"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-recruitment"
        | "com.atlassian.jira-core-project-templates:jira-core-simplified-task-tracking";
      projectTypeKey?: "software" | "service_desk" | "business";
      url?: string;
      [k: string]: any;
    }
    export interface ProjectRoleActorsUpdateBean {
      categorisedActors?: {
        [k: string]: string[];
      };
      id?: number;
      [k: string]: any;
    }
    export interface ProjectTypeBean {
      color?: string;
      descriptionI18nKey?: string;
      formattedKey?: string;
      icon?: string;
      key?: string;
      [k: string]: any;
    }
    export interface PropertyBean {
      id?: string;
      key: string;
      value: string;
      [k: string]: any;
    }
    export interface RemoteIssueLinkBean {
      application?: Application;
      globalId?: string;
      id?: number;
      object?: RemoteObject;
      relationship?: string;
      self?: string;
      [k: string]: any;
    }
    export interface Application {
      name?: string;
      type?: string;
      [k: string]: any;
    }
    export interface RemoteObject {
      icon?: Icon;
      status?: Status;
      summary?: string;
      title: string;
      url: string;
      [k: string]: any;
    }
    export interface Icon {
      link?: string;
      title?: string;
      url16x16?: string;
      [k: string]: any;
    }
    export interface Status {
      icon?: Icon;
      resolved?: boolean;
      [k: string]: any;
    }
    export interface RemoteIssueLinkCreateOrUpdateResponse {
      id?: number;
      self?: string;
      [k: string]: any;
    }
    export interface RemoteIssueLinkRequest {
      application?: Application;
      globalId?: string;
      object: RemoteObject;
      relationship?: string;
      [k: string]: any;
    }
    export interface SimpleErrorCollection {
      errorMessages?: string[];
      errors?: {
        [k: string]: string;
      };
      httpStatusCode?: number;
      [k: string]: any;
    }
    export interface ResolutionBean {
      description?: string;
      id?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface ScreenableFieldBean {
      id?: string;
      name?: string;
      [k: string]: any;
    }
    export interface ScreenableTabBean {
      id?: number;
      name: string;
      [k: string]: any;
    }
    export interface SearchRequestBean {
      expand?: string[];
      fields?: string[];
      fieldsByKeys?: boolean;
      jql: string;
      maxResults?: number;
      properties?: string[];
      startAt?: number;
      validateQuery?: "strict" | "warn" | "none" | "true" | "false";
      [k: string]: any;
    }
    export interface SearchResultsBean {
      expand?: string;
      issues?: IssueBean[];
      maxResults?: number;
      names?: {
        [k: string]: string;
      };
      schema?: {
        [k: string]: JsonTypeBean;
      };
      startAt?: number;
      total?: number;
      warningMessages?: string[];
      [k: string]: any;
    }
    export interface SecurityLevelJsonBean {
      description?: string;
      id?: string;
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface SecurityListLevelJsonBean {
      levels?: SecurityLevelJsonBean[];
      [k: string]: any;
    }
    export interface SecuritySchemeJsonBean {
      defaultSecurityLevelId?: number;
      description?: string;
      id?: number;
      levels?: SecurityLevelJsonBean[];
      name?: string;
      self?: string;
      [k: string]: any;
    }
    export interface SecuritySchemesJsonBean {
      issueSecuritySchemes?: SecuritySchemeJsonBean[];
      [k: string]: any;
    }
    export interface ServerInfoBean {
      baseUrl?: string;
      buildDate?: string;
      buildNumber?: number;
      deploymentType?: string;
      healthChecks?: HealthCheckResult[];
      scmInfo?: string;
      serverTime?: string;
      serverTitle?: string;
      version?: string;
      versionNumbers?: number[];
      [k: string]: any;
    }
    export interface HealthCheckResult {
      description?: string;
      name?: string;
      passed?: boolean;
      [k: string]: any;
    }
    export interface SharePermissionInputBean {
      groupname?: string;
      projectId?: string;
      projectRoleId?: string;
      type: "project" | "group" | "projectRole" | "global" | "authenticated";
      [k: string]: any;
    }
    export interface SimpleApplicationPropertyBean {
      id?: string;
      value?: string;
      [k: string]: any;
    }
    export interface SystemAvatars {
      system?: AvatarBean[];
      [k: string]: any;
    }
    export interface TaskProgressBeanObject {
      description?: string;
      elapsedRuntime: number;
      finished?: number;
      id: string;
      lastUpdate: number;
      message?: string;
      progress: number;
      result?: any;
      self: string;
      started: number;
      status: "ENQUEUED" | "RUNNING" | "COMPLETE" | "FAILED" | "CANCEL_REQUESTED" | "CANCELLED" | "DEAD";
      submitted: number;
      submittedBy: number;
      [k: string]: any;
    }
    export interface TaskProgressBeanRemoveOptionFromIssuesResult {
      description?: string;
      elapsedRuntime: number;
      finished?: number;
      id: string;
      lastUpdate: number;
      message?: string;
      progress: number;
      result?: RemoveOptionFromIssuesResult;
      self: string;
      started: number;
      status: "ENQUEUED" | "RUNNING" | "COMPLETE" | "FAILED" | "CANCEL_REQUESTED" | "CANCELLED" | "DEAD";
      submitted: number;
      submittedBy: number;
      [k: string]: any;
    }
    export interface RemoveOptionFromIssuesResult {
      errors?: SimpleErrorCollection;
      modifiedIssues?: number[];
      unmodifiedIssues?: number[];
      [k: string]: any;
    }
    export interface TimeTrackingImplementationBean {
      key: string;
      name?: string;
      url?: string;
      [k: string]: any;
    }
    export interface TransitionsListBean {
      expand?: string;
      transitions?: TransitionBean[];
      [k: string]: any;
    }
    export interface UpdateUserToGroupBean {
      accountId?: string;
      name?: string;
      [k: string]: any;
    }
    export interface UserPickerResultsBean {
      header?: string;
      total?: number;
      users?: UserPickerUser[];
      [k: string]: any;
    }
    export interface UserPickerUser {
      avatarUrl?: string;
      displayName?: string;
      html?: string;
      key?: string;
      name?: string;
      [k: string]: any;
    }
    export interface UsersAndGroupsBean {
      groups?: GroupSuggestionsBean;
      users?: UserPickerResultsBean;
      [k: string]: any;
    }
    export interface UserWriteBean {
      applicationKeys?: string[];
      displayName: string;
      emailAddress: string;
      key?: string;
      name?: string;
      notification?: string;
      password?: string;
      self?: string;
      [k: string]: any;
    }
    export interface VersionIssueCountsBean {
      customFieldNames?: VersionUsageInCustomFields[];
      issueCountWithCustomFieldsShowingVersion?: number;
      issuesAffectedCount?: number;
      issuesFixedCount?: number;
      self?: string;
      [k: string]: any;
    }
    export interface VersionUsageInCustomFields {
      customFieldId?: number;
      fieldName?: string;
      issueCountWithVersionInCustomField?: number;
      [k: string]: any;
    }
    export interface VersionMoveBean {
      after?: string;
      position?: "Earlier" | "Later" | "First" | "Last";
      [k: string]: any;
    }
    export interface VersionUnresolvedIssueCountsBean {
      issuesCount?: number;
      issuesUnresolvedCount?: number;
      self?: string;
      [k: string]: any;
    }
    export interface VoteBean {
      hasVoted?: boolean;
      self?: string;
      voters?: User[];
      votes?: number;
      [k: string]: any;
    }
    export interface WatchersBean {
      isWatching?: boolean;
      self?: string;
      watchCount?: number;
      watchers?: UserJsonBean[];
      [k: string]: any;
    }
    export interface WorkflowMappingBean {
      defaultMapping?: boolean;
      issueTypes: string[];
      updateDraftIfNeeded?: boolean;
      workflow?: string;
      [k: string]: any;
    }
    export interface WorkflowSchemeBean {
      defaultWorkflow?: string;
      description?: string;
      draft?: boolean;
      id?: number;
      issueTypeMappings?: {
        [k: string]: string;
      };
      issueTypes?: {
        [k: string]: IssueTypeJsonBean;
      };
      lastModified?: string;
      lastModifiedUser?: User;
      name: string;
      originalDefaultWorkflow?: string;
      originalIssueTypeMappings?: {
        [k: string]: string;
      };
      self?: string;
      updateDraftIfNeeded?: boolean;
      [k: string]: any;
    }
    export interface WorklogChangedSinceBean {
      lastPage?: boolean;
      nextPage?: string;
      self?: string;
      since?: number;
      until?: number;
      values?: WorklogChangeBean[];
      [k: string]: any;
    }
    export interface WorklogChangeBean {
      properties?: EntityPropertyBean[];
      updatedTime?: number;
      worklogId?: number;
      [k: string]: any;
    }
    export interface WorklogIdsRequestBean {
      ids: number[];
      [k: string]: any;
    }
    export interface WorklogJsonBean {
      author?: UserJsonBean;
      comment?: string;
      created?: string;
      id?: string;
      issueId?: string;
      properties?: EntityPropertyBean[];
      self?: string;
      started?: string;
      timeSpent?: string;
      timeSpentSeconds?: number;
      updateAuthor?: UserJsonBean;
      updated?: string;
      visibility?: VisibilityJsonBean;
      [k: string]: any;
    }
    export interface WorklogWithPaginationBean {
      maxResults?: number;
      startAt?: number;
      total?: number;
      worklogs?: WorklogJsonBean[];
      [k: string]: any;
    }
  }

  namespace Params {
    export type Empty = {};

    export type ApplicationGetApplicationProperty =
      {
        "key"?: string;
        "keyFilter"?: string;
        "permissionLevel"?: string;
      };
    export type ApplicationSetApplicationProperty =
      {
        "id": string;
        "body": JIRA.Schema.SimpleApplicationPropertyBean;
      };
    export type ApplicationroleGetApplicationRole =
      {
        "key": string;
      };
    export type AttachmentDeleteAttachment =
      {
        "id": string;
      };
    export type AttachmentGetAllMetadataForAnExpandedAttachment =
      {
        "id": string;
      };
    export type AttachmentGetAttachmentMetadata =
      {
        "id": string;
      };
    export type AttachmentGetContentsMetadataForAnExpandedAttachment =
      {
        "id": string;
      };
    export type AuditingGetAuditRecords =
      {
        "filter"?: string;
        "from"?: string;
        "limit"?: number;
        "offset"?: number;
        "to"?: string;
      };
    export type AvatarGetSystemAvatarsByType =
      {
        "type": "issuetype"|"project"|"user";
      };
    export type CommentDeleteCommentProperty =
      {
        "commentId": string;
        "propertyKey": string;
      };
    export type CommentGetCommentProperty =
      {
        "commentId": string;
        "propertyKey": string;
      };
    export type CommentGetCommentPropertyKeys =
      {
        "commentId": string;
      };
    export type CommentGetCommentsByIDs =
      {
        "expand"?: string;
        "body": JIRA.Schema.IssueCommentListRequestBean;
      };
    export type CommentSetCommentProperty =
      {
        "commentId": string;
        "propertyKey": string;
        "body": JIRA.Schema.Any;
      };
    export type ComponentCreateComponent =
      {
        "body": JIRA.Schema.ComponentBean;
      };
    export type ComponentDeleteComponent =
      {
        "id": string;
        "moveIssuesTo"?: string;
      };
    export type ComponentGetComponent =
      {
        "id": string;
      };
    export type ComponentGetComponentIssuesCount =
      {
        "id": string;
      };
    export type ComponentUpdateComponent =
      {
        "id": string;
        "body": JIRA.Schema.ComponentBean;
      };
    export type ConfigurationSelectTimeTrackingProvider =
      {
        "body": JIRA.Schema.TimeTrackingImplementationBean;
      };
    export type ConfigurationSetTimeTrackingSettings =
      {
        "body": JIRA.Schema.TimeTrackingConfigurationBean;
      };
    export type CustomFieldOptionGetCustomFieldOption =
      {
        "id": string;
      };
    export type DashboardDeleteDashboardItemProperty =
      {
        "dashboardId": string;
        "itemId": string;
        "propertyKey": string;
      };
    export type DashboardGetAllDashboards =
      {
        "filter"?: "my"|"favourite";
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type DashboardGetDashboard =
      {
        "id": string;
      };
    export type DashboardGetDashboardItemProperty =
      {
        "dashboardId": string;
        "itemId": string;
        "propertyKey": string;
      };
    export type DashboardGetDashboardItemPropertyKeys =
      {
        "dashboardId": string;
        "itemId": string;
      };
    export type DashboardSetDashboardItemProperty =
      {
        "dashboardId": string;
        "itemId": string;
        "propertyKey": string;
      };
    export type ExpressionEvaluateJiraExpression =
      {
        "expand"?: string;
        "body": JIRA.Schema.JiraExpressionEvalRequestBean;
      };
    export type FieldCreateCustomField =
      {
        "body": JIRA.Schema.CustomFieldDefinitionJsonBean;
      };
    export type FieldCreateIssueFieldOption =
      {
        "fieldKey": string;
        "body": JIRA.Schema.IssueFieldOptionCreateBean;
      };
    export type FieldDeleteIssueFieldOption =
      {
        "fieldKey": string;
        "optionId": number;
      };
    export type FieldGetAllIssueFieldOptions =
      {
        "fieldKey": string;
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type FieldGetIssueFieldOption =
      {
        "fieldKey": string;
        "optionId": number;
      };
    export type FieldGetSelectableIssueFieldOptions =
      {
        "fieldKey": string;
        "maxResults"?: number;
        "projectId"?: number;
        "startAt"?: number;
      };
    export type FieldGetVisibleIssueFieldOptions =
      {
        "fieldKey": string;
        "maxResults"?: number;
        "projectId"?: number;
        "startAt"?: number;
      };
    export type FieldReplaceIssueFieldOption =
      {
        "fieldKey": string;
        "jql"?: string;
        "optionId": number;
        "replaceWith"?: number;
      };
    export type FieldUpdateIssueFieldOption =
      {
        "fieldKey": string;
        "optionId": number;
        "body": JIRA.Schema.IssueFieldOptionBean;
      };
    export type FilterAddFilterAsFavorite =
      {
        "expand"?: string;
        "id": number;
      };
    export type FilterAddSharePermission =
      {
        "id": number;
        "body": JIRA.Schema.SharePermissionInputBean;
      };
    export type FilterCreateFilter =
      {
        "expand"?: "sharedUsers"|"subscriptions";
        "body": JIRA.Schema.FilterBean;
      };
    export type FilterDeleteFilter =
      {
        "id": number;
      };
    export type FilterDeleteSharePermission =
      {
        "id": number;
        "permissionId": number;
      };
    export type FilterGetColumns =
      {
        "id": number;
      };
    export type FilterGetFavouriteFilters =
      {
        "expand"?: string;
      };
    export type FilterGetFilter =
      {
        "expand"?: string;
        "id": number;
      };
    export type FilterGetFilters =
      {
        "expand"?: string;
      };
    export type FilterGetMyFilters =
      {
        "expand"?: string;
        "includeFavourites"?: boolean;
      };
    export type FilterGetSharePermission =
      {
        "id": number;
        "permissionId": number;
      };
    export type FilterGetSharePermissions =
      {
        "id": number;
      };
    export type FilterRemoveFilterAsFavorite =
      {
        "expand"?: string;
        "id": number;
      };
    export type FilterResetColumns =
      {
        "id": number;
      };
    export type FilterSearchForFilters =
      {
        "accountId"?: string;
        "expand"?: string;
        "filterName"?: string;
        "groupname"?: string;
        "maxResults"?: number;
        "orderBy"?: "id"|"name"|"description"|"owner"|"favorite_count"|"is_favorite"|"-id"|"-name"|"-description"|"-owner"|"-favorite_count"|"-is_favorite";
        "owner"?: string;
        "projectId"?: number;
        "startAt"?: number;
      };
    export type FilterSetColumns =
      {
        "id": number;
      };
    export type FilterSetDefaultShareScope =
      {
        "body": JIRA.Schema.DefaultShareScopeBean;
      };
    export type FilterUpdateFilter =
      {
        "expand"?: "sharedUsers"|"subscriptions";
        "id": number;
        "body": JIRA.Schema.FilterBean;
      };
    export type GroupAddUserToGroup =
      {
        "groupname": string;
        "body": JIRA.Schema.UpdateUserToGroupBean;
      };
    export type GroupCreateGroup =
      {
        "body": JIRA.Schema.AddGroupBean;
      };
    export type GroupGetGroup =
      {
        "expand"?: "users";
        "groupname": string;
      };
    export type GroupGetUsersFromGroup =
      {
        "groupname": string;
        "includeInactiveUsers"?: boolean;
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type GroupRemoveGroup =
      {
        "groupname": string;
        "swapGroup"?: string;
      };
    export type GroupRemoveUserFromGroup =
      {
        "accountId"?: string;
        "groupname": string;
        "username"?: string;
      };
    export type GroupsFindGroups =
      {
        "accountId"?: string;
        "exclude"?: string[];
        "maxResults"?: number;
        "query"?: string;
        "userName"?: string;
      };
    export type GroupuserpickerFindUsersAndGroups =
      {
        "avatarSize"?: "xsmall"|"xsmall@2x"|"xsmall@3x"|"small"|"small@2x"|"small@3x"|"medium"|"medium@2x"|"medium@3x"|"large"|"large@2x"|"large@3x"|"xlarge"|"xlarge@2x"|"xlarge@3x"|"xxlarge"|"xxlarge@2x"|"xxlarge@3x"|"xxxlarge"|"xxxlarge@2x"|"xxxlarge@3x";
        "caseInsensitive"?: boolean;
        "excludeConnectAddons"?: boolean;
        "fieldId"?: string;
        "issueTypeId"?: string[];
        "maxResults"?: number;
        "projectId"?: string[];
        "query": string;
        "showAvatar"?: boolean;
      };
    export type IssueAddAttachment =
      {
        "issueIdOrKey": string;
      };
    export type IssueAddComment =
      {
        "expand"?: string;
        "issueIdOrKey": string;
        "body": JIRA.Schema.CommentJsonBean;
      };
    export type IssueAddVote =
      {
        "issueIdOrKey": string;
      };
    export type IssueAddWatcher =
      {
        "issueIdOrKey": string;
      };
    export type IssueAddWorklog =
      {
        "adjustEstimate"?: "new"|"leave"|"manual"|"auto";
        "expand"?: string;
        "issueIdOrKey": string;
        "newEstimate"?: string;
        "notifyUsers"?: boolean;
        "overrideEditableFlag"?: boolean;
        "reduceBy"?: string;
        "body": JIRA.Schema.WorklogJsonBean;
      };
    export type IssueAssignIssue =
      {
        "issueIdOrKey": string;
        "body": JIRA.Schema.User;
      };
    export type IssueBulkDeleteIssueProperty =
      {
        "propertyKey": string;
        "body": JIRA.Schema.EntityPropertyBulkDeleteFilterBean;
      };
    export type IssueBulkIssueCreate =
      {
        "body": JIRA.Schema.IssuesUpdateBean;
      };
    export type IssueBulkSetIssueProperty =
      {
        "propertyKey": string;
        "body": JIRA.Schema.EntityPropertyBulkUpdateRequestBean;
      };
    export type IssueCreateIssue =
      {
        "updateHistory"?: boolean;
        "body": JIRA.Schema.IssueUpdateBean;
      };
    export type IssueCreateOrUpdateRemoteIssueLink =
      {
        "issueIdOrKey": string;
        "body": JIRA.Schema.RemoteIssueLinkRequest;
      };
    export type IssueDeleteComment =
      {
        "id": string;
        "issueIdOrKey": string;
      };
    export type IssueDeleteIssue =
      {
        "deleteSubtasks"?: "true"|"false";
        "issueIdOrKey": string;
      };
    export type IssueDeleteIssueProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
      };
    export type IssueDeleteRemoteIssueLinkByGlobalId =
      {
        "globalId": string;
        "issueIdOrKey": string;
      };
    export type IssueDeleteRemoteIssueLinkById =
      {
        "issueIdOrKey": string;
        "linkId": string;
      };
    export type IssueDeleteVote =
      {
        "issueIdOrKey": string;
      };
    export type IssueDeleteWatcher =
      {
        "accountId"?: string;
        "issueIdOrKey": string;
        "username"?: string;
      };
    export type IssueDeleteWorklog =
      {
        "adjustEstimate"?: "new"|"leave"|"manual"|"auto";
        "id": string;
        "increaseBy"?: string;
        "issueIdOrKey": string;
        "newEstimate"?: string;
        "notifyUsers"?: boolean;
        "overrideEditableFlag"?: boolean;
      };
    export type IssueDeleteWorklogProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
        "worklogId": string;
      };
    export type IssueEditIssue =
      {
        "issueIdOrKey": string;
        "notifyUsers"?: boolean;
        "overrideEditableFlag"?: boolean;
        "overrideScreenSecurity"?: boolean;
        "body": JIRA.Schema.IssueUpdateBean;
      };
    export type IssueGetChangeLogs =
      {
        "issueIdOrKey": string;
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type IssueGetComment =
      {
        "expand"?: string;
        "id": string;
        "issueIdOrKey": string;
      };
    export type IssueGetComments =
      {
        "expand"?: string;
        "issueIdOrKey": string;
        "maxResults"?: number;
        "orderBy"?: "created";
        "startAt"?: number;
      };
    export type IssueGetCreateIssueMetadata =
      {
        "expand"?: string;
        "issuetypeIds"?: string[];
        "issuetypeNames"?: string[];
        "projectIds"?: string[];
        "projectKeys"?: string[];
      };
    export type IssueGetEditIssueMetadata =
      {
        "issueIdOrKey": string;
        "overrideEditableFlag"?: boolean;
        "overrideScreenSecurity"?: boolean;
      };
    export type IssueGetIssue =
      {
        "expand"?: string;
        "fields"?: string[];
        "fieldsByKeys"?: boolean;
        "issueIdOrKey": string;
        "properties"?: string[];
        "updateHistory"?: boolean;
      };
    export type IssueGetIssuePickerSuggestions =
      {
        "currentIssueKey"?: string;
        "currentJQL"?: string;
        "currentProjectId"?: string;
        "query"?: string;
        "showSubTaskParent"?: boolean;
        "showSubTasks"?: boolean;
      };
    export type IssueGetIssueProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
      };
    export type IssueGetIssuePropertyKeys =
      {
        "issueIdOrKey": string;
      };
    export type IssueGetIssueWatchers =
      {
        "issueIdOrKey": string;
      };
    export type IssueGetIssueWorklogs =
      {
        "expand"?: string;
        "issueIdOrKey": string;
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type IssueGetRemoteIssueLinkById =
      {
        "issueIdOrKey": string;
        "linkId": string;
      };
    export type IssueGetRemoteIssueLinks =
      {
        "globalId"?: string;
        "issueIdOrKey": string;
      };
    export type IssueGetTransitions =
      {
        "expand"?: string;
        "issueIdOrKey": string;
        "skipRemoteOnlyCondition"?: boolean;
        "transitionId"?: string;
      };
    export type IssueGetVotes =
      {
        "issueIdOrKey": string;
      };
    export type IssueGetWorklog =
      {
        "expand"?: string;
        "id": string;
        "issueIdOrKey": string;
      };
    export type IssueGetWorklogProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
        "worklogId": string;
      };
    export type IssueGetWorklogPropertyKeys =
      {
        "issueIdOrKey": string;
        "worklogId": string;
      };
    export type IssueSendNotificationForIssue =
      {
        "issueIdOrKey": string;
        "body": JIRA.Schema.NotificationJsonBean;
      };
    export type IssueSetIssueProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
        "body": JIRA.Schema.Any;
      };
    export type IssueSetWorklogProperty =
      {
        "issueIdOrKey": string;
        "propertyKey": string;
        "worklogId": string;
        "body": JIRA.Schema.Any;
      };
    export type IssueTransitionIssue =
      {
        "issueIdOrKey": string;
        "body": JIRA.Schema.IssueUpdateBean;
      };
    export type IssueUpdateComment =
      {
        "expand"?: string;
        "id": string;
        "issueIdOrKey": string;
        "body": JIRA.Schema.CommentJsonBean;
      };
    export type IssueUpdateRemoteIssueLink =
      {
        "issueIdOrKey": string;
        "linkId": string;
        "body": JIRA.Schema.RemoteIssueLinkRequest;
      };
    export type IssueUpdateWorklog =
      {
        "adjustEstimate"?: "new"|"leave"|"auto";
        "expand"?: string;
        "id": string;
        "issueIdOrKey": string;
        "newEstimate"?: string;
        "notifyUsers"?: boolean;
        "overrideEditableFlag"?: boolean;
        "body": JIRA.Schema.WorklogJsonBean;
      };
    export type IssueLinkCreateIssueLink =
      {
        "body": JIRA.Schema.LinkIssueRequestJsonBean;
      };
    export type IssueLinkDeleteIssueLink =
      {
        "linkId": string;
      };
    export type IssueLinkGetIssueLink =
      {
        "linkId": string;
      };
    export type IssueLinkTypeCreateIssueLinkType =
      {
        "body": JIRA.Schema.IssueLinkTypeJsonBean;
      };
    export type IssueLinkTypeDeleteIssueLinkType =
      {
        "issueLinkTypeId": string;
      };
    export type IssueLinkTypeGetIssueLinkType =
      {
        "issueLinkTypeId": string;
      };
    export type IssueLinkTypeUpdateIssueLinkType =
      {
        "issueLinkTypeId": string;
        "body": JIRA.Schema.IssueLinkTypeJsonBean;
      };
    export type IssuesecurityschemesGetIssueSecurityScheme =
      {
        "id": number;
      };
    export type IssuetypeCreateIssueType =
      {
        "body": JIRA.Schema.IssueTypeCreateBean;
      };
    export type IssuetypeDeleteIssueType =
      {
        "alternativeIssueTypeId"?: string;
        "id": string;
      };
    export type IssuetypeDeleteIssueTypeProperty =
      {
        "issueTypeId": string;
        "propertyKey": string;
      };
    export type IssuetypeGetAlternativeIssueTypes =
      {
        "id": string;
      };
    export type IssuetypeGetIssueType =
      {
        "id": string;
      };
    export type IssuetypeGetIssueTypeProperty =
      {
        "issueTypeId": string;
        "propertyKey": string;
      };
    export type IssuetypeGetIssueTypePropertyKeys =
      {
        "issueTypeId": string;
      };
    export type IssuetypeLoadIssueTypeAvatar =
      {
        "id": string;
        "size": number;
        "x"?: number;
        "y"?: number;
        "body": JIRA.Schema.Any;
      };
    export type IssuetypeSetIssueTypeProperty =
      {
        "issueTypeId": string;
        "propertyKey": string;
        "body": JIRA.Schema.Any;
      };
    export type IssuetypeUpdateIssueType =
      {
        "id": string;
        "body": JIRA.Schema.IssueTypeUpdateBean;
      };
    export type JqlConvertUserIdentifiersToAccountIDsInJqlQueries =
      {
        "body": JIRA.Schema.JqlPersonalDataMigrationRequest;
      };
    export type JqlGetFieldAutoCompleteSuggestions =
      {
        "fieldName"?: string;
        "fieldValue"?: string;
        "predicateName"?: string;
        "predicateValue"?: string;
      };
    export type MypermissionsGetMyPermissions =
      {
        "issueId"?: string;
        "issueKey"?: string;
        "permissions"?: string;
        "projectId"?: string;
        "projectKey"?: string;
      };
    export type MypreferencesDeletePreference =
      {
        "key": string;
      };
    export type MypreferencesGetPreference =
      {
        "key": string;
      };
    export type MypreferencesSetLocale =
      {
        "body": JIRA.Schema.LocaleBean;
      };
    export type MypreferencesSetPreference =
      {
        "key": string;
      };
    export type MyselfGetCurrentUser =
      {
        "expand"?: string;
      };
    export type NotificationschemeGetNotificationScheme =
      {
        "expand"?: string;
        "id": number;
      };
    export type NotificationschemeGetNotificationSchemesPaginated =
      {
        "expand"?: string;
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type PermissionsGetPermittedProjects =
      {
        "body": JIRA.Schema.PermissionsKeysBean;
      };
    export type PermissionschemeCreatePermissionGrant =
      {
        "expand"?: string;
        "schemeId": number;
        "body": JIRA.Schema.PermissionGrantBean;
      };
    export type PermissionschemeCreatePermissionScheme =
      {
        "expand"?: string;
        "body": JIRA.Schema.PermissionSchemeBean;
      };
    export type PermissionschemeDeletePermissionScheme =
      {
        "schemeId": number;
      };
    export type PermissionschemeDeletePermissionSchemeEntity =
      {
        "permissionId": number;
        "schemeId": number;
      };
    export type PermissionschemeGetAllPermissionSchemes =
      {
        "expand"?: string;
      };
    export type PermissionschemeGetPermissionScheme =
      {
        "expand"?: string;
        "schemeId": number;
      };
    export type PermissionschemeGetPermissionSchemeGrant =
      {
        "expand"?: string;
        "permissionId": number;
        "schemeId": number;
      };
    export type PermissionschemeGetPermissionSchemeGrants =
      {
        "expand"?: string;
        "schemeId": number;
      };
    export type PermissionschemeUpdatePermissionScheme =
      {
        "expand"?: string;
        "schemeId": number;
        "body": JIRA.Schema.PermissionSchemeBean;
      };
    export type PriorityGetPriority =
      {
        "id": string;
      };
    export type ProjectAddActorsToProjectRole =
      {
        "id": number;
        "projectIdOrKey": string;
        "body": JIRA.Schema.ActorsMap;
      };
    export type ProjectAssignPermissionScheme =
      {
        "expand"?: string;
        "projectKeyOrId": string;
        "body": JIRA.Schema.IdBean;
      };
    export type ProjectCreateProject =
      {
        "body": JIRA.Schema.ProjectInputBean;
      };
    export type ProjectDeleteActorsFromProjectRole =
      {
        "group"?: string;
        "id": number;
        "projectIdOrKey": string;
        "user"?: string;
      };
    export type ProjectDeleteProject =
      {
        "projectIdOrKey": string;
      };
    export type ProjectDeleteProjectAvatar =
      {
        "id": number;
        "projectIdOrKey": string;
      };
    export type ProjectDeleteProjectProperty =
      {
        "projectIdOrKey": string;
        "propertyKey": string;
      };
    export type ProjectGetAccessibleProjectTypeByKey =
      {
        "projectTypeKey": "business"|"service_desk"|"software";
      };
    export type ProjectGetAllProjectAvatars =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetAllProjects =
      {
        "expand"?: string;
        "recent"?: number;
      };
    export type ProjectGetAllStatusesForProject =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetAssignedPermissionScheme =
      {
        "expand"?: string;
        "projectKeyOrId": string;
      };
    export type ProjectGetProject =
      {
        "expand"?: string;
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectComponents =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectComponentsPaginated =
      {
        "maxResults"?: number;
        "orderBy"?: "description"|"-description"|"+description"|"issueCount"|"-issueCount"|"+issueCount"|"lead"|"-lead"|"+lead"|"name"|"-name"|"+name";
        "projectIdOrKey": string;
        "query"?: string;
        "startAt"?: number;
      };
    export type ProjectGetProjectIssueSecurityLevels =
      {
        "projectKeyOrId": string;
      };
    export type ProjectGetProjectIssueSecurityScheme =
      {
        "projectKeyOrId": string;
      };
    export type ProjectGetProjectNotificationScheme =
      {
        "expand"?: string;
        "projectKeyOrId": string;
      };
    export type ProjectGetProjectProperty =
      {
        "projectIdOrKey": string;
        "propertyKey": string;
      };
    export type ProjectGetProjectPropertyKeys =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectRoleDetails =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectRoleForProject =
      {
        "id": number;
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectRolesForProject =
      {
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectsPaginated =
      {
        "action"?: "view"|"edit";
        "categoryId"?: number;
        "expand"?: string;
        "maxResults"?: number;
        "orderBy"?: "category"|"-category"|"+category"|"key"|"-key"|"+key"|"name"|"-name"|"+name"|"owner"|"-owner"|"+owner";
        "query"?: string;
        "startAt"?: number;
        "typeKey"?: string;
      };
    export type ProjectGetProjectTypeByKey =
      {
        "projectTypeKey": "business"|"service_desk"|"software";
      };
    export type ProjectGetProjectVersions =
      {
        "expand"?: string;
        "projectIdOrKey": string;
      };
    export type ProjectGetProjectVersionsPaginated =
      {
        "expand"?: string;
        "maxResults"?: number;
        "orderBy"?: "description"|"-description"|"+description"|"name"|"-name"|"+name"|"releaseDate"|"-releaseDate"|"+releaseDate"|"sequence"|"-sequence"|"+sequence"|"startDate"|"-startDate"|"+startDate";
        "projectIdOrKey": string;
        "query"?: string;
        "startAt"?: number;
        "status"?: "released"|"unreleased"|"archived";
      };
    export type ProjectLoadProjectAvatar =
      {
        "projectIdOrKey": string;
        "size"?: number;
        "x"?: number;
        "y"?: number;
        "body": JIRA.Schema.Any;
      };
    export type ProjectSetActorsForProjectRole =
      {
        "id": number;
        "projectIdOrKey": string;
        "body": JIRA.Schema.ProjectRoleActorsUpdateBean;
      };
    export type ProjectSetProjectAvatar =
      {
        "projectIdOrKey": string;
        "body": JIRA.Schema.AvatarBean;
      };
    export type ProjectSetProjectProperty =
      {
        "projectIdOrKey": string;
        "propertyKey": string;
        "body": JIRA.Schema.Any;
      };
    export type ProjectUpdateProject =
      {
        "expand"?: string;
        "projectIdOrKey": string;
        "body": JIRA.Schema.ProjectInputBean;
      };
    export type ProjectUpdateProjectType =
      {
        "newProjectTypeKey": "software"|"service_desk"|"business";
        "projectIdOrKey": string;
      };
    export type ProjectCategoryCreateProjectCategory =
      {
        "body": JIRA.Schema.ProjectCategoryBean;
      };
    export type ProjectCategoryDeleteProjectCategory =
      {
        "id": number;
      };
    export type ProjectCategoryGetProjectCategoryById =
      {
        "id": number;
      };
    export type ProjectCategoryUpdateProjectCategory =
      {
        "id": number;
        "body": JIRA.Schema.ProjectCategoryBean;
      };
    export type ProjectvalidateGetValidProjectKey =
      {
        "key"?: string;
      };
    export type ProjectvalidateGetValidProjectName =
      {
        "name": string;
      };
    export type ProjectvalidateValidateProjectKey =
      {
        "key"?: string;
      };
    export type ResolutionGetResolution =
      {
        "id": string;
      };
    export type RoleAddDefaultActorsToProjectRole =
      {
        "id": number;
        "body": JIRA.Schema.ActorInputBean;
      };
    export type RoleCreateProjectRole =
      {
        "body": JIRA.Schema.CreateUpdateRoleRequestBean;
      };
    export type RoleDeleteDefaultActorsFromProjectRole =
      {
        "group"?: string;
        "id": number;
        "user"?: string;
      };
    export type RoleDeleteProjectRole =
      {
        "id": number;
        "swap"?: number;
      };
    export type RoleFullyUpdateProjectRole =
      {
        "id": number;
        "body": JIRA.Schema.CreateUpdateRoleRequestBean;
      };
    export type RoleGetDefaultActorsForProjectRole =
      {
        "id": number;
      };
    export type RoleGetProjectRoleById =
      {
        "id": number;
      };
    export type RolePartialUpdateProjectRole =
      {
        "id": number;
        "body": JIRA.Schema.CreateUpdateRoleRequestBean;
      };
    export type ScreensAddFieldToDefaultScreen =
      {
        "fieldId": string;
      };
    export type ScreensAddScreenTabField =
      {
        "screenId": number;
        "tabId": number;
        "body": JIRA.Schema.AddFieldBean;
      };
    export type ScreensCreateScreenTab =
      {
        "screenId": number;
        "body": JIRA.Schema.ScreenableTabBean;
      };
    export type ScreensDeleteScreenTab =
      {
        "screenId": number;
        "tabId": number;
      };
    export type ScreensGetAllScreens =
      {
        "maxResults"?: number;
        "startAt"?: number;
      };
    export type ScreensGetAllScreenTabFields =
      {
        "projectKey"?: string;
        "screenId": number;
        "tabId": number;
      };
    export type ScreensGetAllScreenTabs =
      {
        "projectKey"?: string;
        "screenId": number;
      };
    export type ScreensGetAvailableScreenFields =
      {
        "screenId": number;
      };
    export type ScreensMoveScreenTab =
      {
        "pos": number;
        "screenId": number;
        "tabId": number;
      };
    export type ScreensMoveScreenTabField =
      {
        "id": string;
        "screenId": number;
        "tabId": number;
        "body": JIRA.Schema.MoveFieldBean;
      };
    export type ScreensRemoveScreenTabField =
      {
        "id": string;
        "screenId": number;
        "tabId": number;
      };
    export type ScreensUpdateScreenTab =
      {
        "screenId": number;
        "tabId": number;
        "body": JIRA.Schema.ScreenableTabBean;
      };
    export type SearchSearchForIssuesUsingJqlGet =
      {
        "expand"?: string;
        "fields"?: string[];
        "fieldsByKeys"?: boolean;
        "jql"?: string;
        "maxResults"?: number;
        "properties"?: string[];
        "startAt"?: number;
        "validateQuery"?: "strict"|"warn"|"none"|"true"|"false";
      };
    export type SearchSearchForIssuesUsingJqlPost =
      {
        "body": JIRA.Schema.SearchRequestBean;
      };
    export type SecuritylevelGetIssueSecurityLevel =
      {
        "id": string;
      };
    export type SessionCreateSession =
      {
        "body": JIRA.Schema.AuthParams;
      };
    export type StatusGetStatus =
      {
        "idOrName": string;
      };
    export type StatuscategoryGetStatusCategory =
      {
        "idOrKey": string;
      };
    export type TaskCancelTask =
      {
        "taskId": string;
      };
    export type TaskGetTask =
      {
        "taskId": string;
      };
    export type UniversalAvatarDeleteAvatar =
      {
        "id": number;
        "owningObjectId": string;
        "type": string;
      };
    export type UniversalAvatarGetAvatars =
      {
        "entityId": string;
        "type": string;
      };
    export type UniversalAvatarLoadAvatar =
      {
        "entityId": string;
        "size": number;
        "type": string;
        "x"?: number;
        "y"?: number;
        "body": JIRA.Schema.Any;
      };
    export type UserBulkGetUsers =
      {
        "accountId"?: string[];
        "key"?: string[];
        "maxResults"?: number;
        "startAt"?: number;
        "username"?: string[];
      };
    export type UserCreateUser =
      {
        "body": JIRA.Schema.UserWriteBean;
      };
    export type UserDeleteUser =
      {
        "accountId"?: string;
        "key"?: string;
        "username"?: string;
      };
    export type UserDeleteUserProperty =
      {
        "accountId"?: string;
        "propertyKey": string;
        "userKey"?: string;
        "username"?: string;
      };
    export type UserFindUserKeysByQuery =
      {
        "includeInactive"?: boolean;
        "maxResults"?: number;
        "query": string;
        "startAt"?: number;
      };
    export type UserFindUsers =
      {
        "includeActive"?: boolean;
        "includeInactive"?: boolean;
        "maxResults"?: number;
        "property"?: string;
        "query"?: string;
        "startAt"?: number;
        "username"?: string;
      };
    export type UserFindUsersAssignableToIssues =
      {
        "actionDescriptorId"?: number;
        "issueKey"?: string;
        "maxResults"?: number;
        "project"?: string;
        "query"?: string;
        "startAt"?: number;
        "username"?: string;
      };
    export type UserFindUsersAssignableToProjects =
      {
        "maxResults"?: number;
        "projectKeys": string;
        "query"?: string;
        "startAt"?: number;
        "username"?: string;
      };
    export type UserFindUsersByQuery =
      {
        "includeInactive"?: boolean;
        "maxResults"?: number;
        "query": string;
        "startAt"?: number;
      };
    export type UserFindUsersForPicker =
      {
        "exclude"?: string[];
        "excludeAccountIds"?: string[];
        "maxResults"?: number;
        "query": string;
        "showAvatar"?: boolean;
      };
    export type UserFindUsersWithBrowsePermission =
      {
        "issueKey"?: string;
        "maxResults"?: number;
        "projectKey"?: string;
        "query"?: string;
        "startAt"?: number;
        "username"?: string;
      };
    export type UserFindUsersWithPermissions =
      {
        "issueKey"?: string;
        "maxResults"?: number;
        "permissions": string;
        "projectKey"?: string;
        "query"?: string;
        "startAt"?: number;
        "username"?: string;
      };
    export type UserGetUser =
      {
        "accountId"?: string;
        "expand"?: string;
        "key"?: string;
        "username"?: string;
      };
    export type UserGetUserDefaultColumns =
      {
        "accountId"?: string;
        "username"?: string;
      };
    export type UserGetUserGroups =
      {
        "accountId"?: string;
        "key"?: string;
        "username"?: string;
      };
    export type UserGetUserProperty =
      {
        "accountId"?: string;
        "propertyKey": string;
        "userKey"?: string;
        "username"?: string;
      };
    export type UserGetUserPropertyKeys =
      {
        "accountId"?: string;
        "userKey"?: string;
        "username"?: string;
      };
    export type UserResetUserDefaultColumns =
      {
        "accountId"?: string;
        "username"?: string;
      };
    export type UserSetUserDefaultColumns =
      {
        "accountId"?: string;
      };
    export type UserSetUserProperty =
      {
        "accountId"?: string;
        "propertyKey": string;
        "userKey"?: string;
        "username"?: string;
        "body": JIRA.Schema.Any;
      };
    export type VersionCreateVersion =
      {
        "body": JIRA.Schema.VersionBean;
      };
    export type VersionDeleteAndReplaceVersion =
      {
        "id": string;
        "body": JIRA.Schema.DeleteAndReplaceVersionBean;
      };
    export type VersionDeleteVersion =
      {
        "id": string;
        "moveAffectedIssuesTo"?: string;
        "moveFixIssuesTo"?: string;
      };
    export type VersionGetVersion =
      {
        "expand"?: string;
        "id": string;
      };
    export type VersionGetVersionsRelatedIssuesCount =
      {
        "id": string;
      };
    export type VersionGetVersionsUnresolvedIssuesCount =
      {
        "id": string;
      };
    export type VersionMergeVersions =
      {
        "id": string;
        "moveIssuesTo": string;
      };
    export type VersionMoveVersion =
      {
        "id": string;
        "body": JIRA.Schema.VersionMoveBean;
      };
    export type VersionUpdateVersion =
      {
        "id": string;
        "body": JIRA.Schema.VersionBean;
      };
    export type WorkflowCreateWorkflowTransitionProperty =
      {
        "key": string;
        "transitionId": number;
        "workflowMode"?: "live"|"draft";
        "workflowName": string;
        "body": JIRA.Schema.PropertyBean;
      };
    export type WorkflowDeleteWorkflowTransitionProperty =
      {
        "key": string;
        "transitionId": number;
        "workflowMode"?: "live"|"draft";
        "workflowName": string;
      };
    export type WorkflowGetAllWorkflows =
      {
        "workflowName"?: string;
      };
    export type WorkflowGetWorkflowTransitionProperties =
      {
        "includeReservedKeys"?: boolean;
        "key"?: string;
        "transitionId": number;
        "workflowMode"?: "live"|"draft";
        "workflowName": string;
      };
    export type WorkflowUpdateWorkflowTransitionProperty =
      {
        "key": string;
        "transitionId": number;
        "workflowMode"?: "live"|"draft";
        "workflowName": string;
        "body": JIRA.Schema.PropertyBean;
      };
    export type WorkflowschemeCreateDraftWorkflowScheme =
      {
        "id": number;
      };
    export type WorkflowschemeCreateWorkflowScheme =
      {
        "body": JIRA.Schema.WorkflowSchemeBean;
      };
    export type WorkflowschemeDeleteDefaultWorkflow =
      {
        "id": number;
        "updateDraftIfNeeded"?: boolean;
      };
    export type WorkflowschemeDeleteDraftDefaultWorkflow =
      {
        "id": number;
      };
    export type WorkflowschemeDeleteDraftWorkflowScheme =
      {
        "id": number;
      };
    export type WorkflowschemeDeleteIssueTypesForWorkflowInDraftWorkflowScheme =
      {
        "id": number;
        "workflowName": string;
      };
    export type WorkflowschemeDeleteIssueTypesForWorkflowInWorkflowScheme =
      {
        "id": number;
        "updateDraftIfNeeded"?: boolean;
        "workflowName": string;
      };
    export type WorkflowschemeDeleteWorkflowForIssueTypeInDraftWorkflowScheme =
      {
        "id": number;
        "issueType": string;
      };
    export type WorkflowschemeDeleteWorkflowForIssueTypeInWorkflowScheme =
      {
        "id": number;
        "issueType": string;
        "updateDraftIfNeeded"?: boolean;
      };
    export type WorkflowschemeDeleteWorkflowScheme =
      {
        "id": number;
      };
    export type WorkflowschemeGetDefaultWorkflow =
      {
        "id": number;
        "returnDraftIfExists"?: boolean;
      };
    export type WorkflowschemeGetDraftDefaultWorkflow =
      {
        "id": number;
      };
    export type WorkflowschemeGetDraftWorkflowScheme =
      {
        "id": number;
      };
    export type WorkflowschemeGetIssueTypesForWorkflowsInDraftWorkflowScheme =
      {
        "id": number;
        "workflowName"?: string;
      };
    export type WorkflowschemeGetIssueTypesForWorkflowsInWorkflowScheme =
      {
        "id": number;
        "returnDraftIfExists"?: boolean;
        "workflowName"?: string;
      };
    export type WorkflowschemeGetWorkflowForIssueTypeInDraftWorkflowScheme =
      {
        "id": number;
        "issueType": string;
      };
    export type WorkflowschemeGetWorkflowForIssueTypeInWorkflowScheme =
      {
        "id": number;
        "issueType": string;
        "returnDraftIfExists"?: boolean;
      };
    export type WorkflowschemeGetWorkflowScheme =
      {
        "id": number;
        "returnDraftIfExists"?: boolean;
      };
    export type WorkflowschemeSetIssueTypesForWorkflowInWorkflowScheme =
      {
        "id": number;
        "workflowName": string;
        "body": JIRA.Schema.WorkflowMappingBean;
      };
    export type WorkflowschemeSetWorkflowForIssueTypeInDraftWorkflowScheme =
      {
        "id": number;
        "issueType": string;
        "body": JIRA.Schema.IssueTypeMappingBean;
      };
    export type WorkflowschemeSetWorkflowForIssueTypeInWorkflowScheme =
      {
        "id": number;
        "issueType": string;
        "body": JIRA.Schema.IssueTypeMappingBean;
      };
    export type WorkflowschemeUpdateDefaultWorkflow =
      {
        "id": number;
        "body": JIRA.Schema.DefaultBean;
      };
    export type WorkflowschemeUpdateDraftDefaultWorkflow =
      {
        "id": number;
        "body": JIRA.Schema.DefaultBean;
      };
    export type WorkflowschemeUpdateDraftWorkflowScheme =
      {
        "id": number;
        "body": JIRA.Schema.WorkflowSchemeBean;
      };
    export type WorkflowschemeUpdateWorkflowScheme =
      {
        "id": number;
        "body": JIRA.Schema.WorkflowSchemeBean;
      };
    export type WorklogGetIDsOfDeletedWorklogs =
      {
        "since"?: number;
      };
    export type WorklogGetIDsOfUpdatedWorklogs =
      {
        "expand"?: string;
        "since"?: number;
      };
    export type WorklogGetWorklogs =
      {
        "expand"?: string;
        "body": JIRA.Schema.WorklogIdsRequestBean;
      };
  }
}

declare class JIRA {
  constructor(options?: JIRA.Options);

  authenticate(auth: JIRA.Auth): void;
  
  application: {
    getAdvancedSettings(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ApplicationPropertyBean[]>>): Promise<JIRA.Response<JIRA.Schema.ApplicationPropertyBean[]>>;
    getApplicationProperty(params: JIRA.Params.ApplicationGetApplicationProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ApplicationPropertyBean[]>>): Promise<JIRA.Response<JIRA.Schema.ApplicationPropertyBean[]>>;
    setApplicationProperty(params: JIRA.Params.ApplicationSetApplicationProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ApplicationPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.ApplicationPropertyBean>>;
  };
  applicationrole: {
    getAllApplicationRoles(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ApplicationRoleBean[]>>): Promise<JIRA.Response<JIRA.Schema.ApplicationRoleBean[]>>;
    getApplicationRole(params: JIRA.Params.ApplicationroleGetApplicationRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ApplicationRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ApplicationRoleBean>>;
  };
  attachment: {
    deleteAttachment(params: JIRA.Params.AttachmentDeleteAttachment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllMetadataForAnExpandedAttachment(params: JIRA.Params.AttachmentGetAllMetadataForAnExpandedAttachment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.HumanReadableArchive>>): Promise<JIRA.Response<JIRA.Schema.HumanReadableArchive>>;
    getAttachmentMetadata(params: JIRA.Params.AttachmentGetAttachmentMetadata, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AttachmentBean>>): Promise<JIRA.Response<JIRA.Schema.AttachmentBean>>;
    getContentsMetadataForAnExpandedAttachment(params: JIRA.Params.AttachmentGetContentsMetadataForAnExpandedAttachment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AttachmentArchiveImpl>>): Promise<JIRA.Response<JIRA.Schema.AttachmentArchiveImpl>>;
    getGlobalAttachmentSettings(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AttachmentMetaBean>>): Promise<JIRA.Response<JIRA.Schema.AttachmentMetaBean>>;
  };
  auditing: {
    getAuditRecords(params: JIRA.Params.AuditingGetAuditRecords, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AuditingResponseBean>>): Promise<JIRA.Response<JIRA.Schema.AuditingResponseBean>>;
  };
  avatar: {
    getSystemAvatarsByType(params: JIRA.Params.AvatarGetSystemAvatarsByType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SystemAvatars>>): Promise<JIRA.Response<JIRA.Schema.SystemAvatars>>;
  };
  comment: {
    deleteCommentProperty(params: JIRA.Params.CommentDeleteCommentProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getCommentProperty(params: JIRA.Params.CommentGetCommentProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getCommentPropertyKeys(params: JIRA.Params.CommentGetCommentPropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    getCommentsByIDs(params: JIRA.Params.CommentGetCommentsByIDs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanCommentJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanCommentJsonBean>>;
    setCommentProperty(params: JIRA.Params.CommentSetCommentProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  component: {
    createComponent(params: JIRA.Params.ComponentCreateComponent, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ComponentBean>>): Promise<JIRA.Response<JIRA.Schema.ComponentBean>>;
    deleteComponent(params: JIRA.Params.ComponentDeleteComponent, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getComponent(params: JIRA.Params.ComponentGetComponent, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ComponentBean>>): Promise<JIRA.Response<JIRA.Schema.ComponentBean>>;
    getComponentIssuesCount(params: JIRA.Params.ComponentGetComponentIssuesCount, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ComponentIssueCountsBean>>): Promise<JIRA.Response<JIRA.Schema.ComponentIssueCountsBean>>;
    updateComponent(params: JIRA.Params.ComponentUpdateComponent, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ComponentBean>>): Promise<JIRA.Response<JIRA.Schema.ComponentBean>>;
  };
  configuration: {
    disableTimeTracking(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllTimeTrackingProviders(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TimeTrackingImplementationBean[]>>): Promise<JIRA.Response<JIRA.Schema.TimeTrackingImplementationBean[]>>;
    getGlobalSettings(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ConfigurationBean>>): Promise<JIRA.Response<JIRA.Schema.ConfigurationBean>>;
    getSelectedTimeTrackingProvider(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TimeTrackingImplementationBean>>): Promise<JIRA.Response<JIRA.Schema.TimeTrackingImplementationBean>>;
    getTimeTrackingSettings(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TimeTrackingConfigurationBean>>): Promise<JIRA.Response<JIRA.Schema.TimeTrackingConfigurationBean>>;
    selectTimeTrackingProvider(params: JIRA.Params.ConfigurationSelectTimeTrackingProvider, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setTimeTrackingSettings(params: JIRA.Params.ConfigurationSetTimeTrackingSettings, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TimeTrackingConfigurationBean>>): Promise<JIRA.Response<JIRA.Schema.TimeTrackingConfigurationBean>>;
  };
  customFieldOption: {
    getCustomFieldOption(params: JIRA.Params.CustomFieldOptionGetCustomFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CustomFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.CustomFieldOptionBean>>;
  };
  dashboard: {
    deleteDashboardItemProperty(params: JIRA.Params.DashboardDeleteDashboardItemProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllDashboards(params: JIRA.Params.DashboardGetAllDashboards, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DashboardsBean>>): Promise<JIRA.Response<JIRA.Schema.DashboardsBean>>;
    getDashboard(params: JIRA.Params.DashboardGetDashboard, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DashboardBean>>): Promise<JIRA.Response<JIRA.Schema.DashboardBean>>;
    getDashboardItemProperty(params: JIRA.Params.DashboardGetDashboardItemProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getDashboardItemPropertyKeys(params: JIRA.Params.DashboardGetDashboardItemPropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    setDashboardItemProperty(params: JIRA.Params.DashboardSetDashboardItemProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  expression: {
    evaluateJiraExpression(params: JIRA.Params.ExpressionEvaluateJiraExpression, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.JiraExpressionResultBean>>): Promise<JIRA.Response<JIRA.Schema.JiraExpressionResultBean>>;
  };
  field: {
    createCustomField(params: JIRA.Params.FieldCreateCustomField, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FieldBean>>): Promise<JIRA.Response<JIRA.Schema.FieldBean>>;
    createIssueFieldOption(params: JIRA.Params.FieldCreateIssueFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>;
    deleteIssueFieldOption(params: JIRA.Params.FieldDeleteIssueFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllIssueFieldOptions(params: JIRA.Params.FieldGetAllIssueFieldOptions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>;
    getFields(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FieldBean[]>>): Promise<JIRA.Response<JIRA.Schema.FieldBean[]>>;
    getIssueFieldOption(params: JIRA.Params.FieldGetIssueFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>;
    getSelectableIssueFieldOptions(params: JIRA.Params.FieldGetSelectableIssueFieldOptions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>;
    getVisibleIssueFieldOptions(params: JIRA.Params.FieldGetVisibleIssueFieldOptions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanIssueFieldOptionBean>>;
    replaceIssueFieldOption(params: JIRA.Params.FieldReplaceIssueFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TaskProgressBeanRemoveOptionFromIssuesResult>>): Promise<JIRA.Response<JIRA.Schema.TaskProgressBeanRemoveOptionFromIssuesResult>>;
    updateIssueFieldOption(params: JIRA.Params.FieldUpdateIssueFieldOption, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>): Promise<JIRA.Response<JIRA.Schema.IssueFieldOptionBean>>;
  };
  filter: {
    addFilterAsFavorite(params: JIRA.Params.FilterAddFilterAsFavorite, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean>>): Promise<JIRA.Response<JIRA.Schema.FilterBean>>;
    addSharePermission(params: JIRA.Params.FilterAddSharePermission, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterPermissionBean[]>>): Promise<JIRA.Response<JIRA.Schema.FilterPermissionBean[]>>;
    createFilter(params: JIRA.Params.FilterCreateFilter, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean>>): Promise<JIRA.Response<JIRA.Schema.FilterBean>>;
    deleteFilter(params: JIRA.Params.FilterDeleteFilter, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteSharePermission(params: JIRA.Params.FilterDeleteSharePermission, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getColumns(params: JIRA.Params.FilterGetColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ColumnItem[]>>): Promise<JIRA.Response<JIRA.Schema.ColumnItem[]>>;
    getDefaultShareScope(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DefaultShareScopeBean>>): Promise<JIRA.Response<JIRA.Schema.DefaultShareScopeBean>>;
    getFavouriteFilters(params: JIRA.Params.FilterGetFavouriteFilters, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean[]>>): Promise<JIRA.Response<JIRA.Schema.FilterBean[]>>;
    getFilter(params: JIRA.Params.FilterGetFilter, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean>>): Promise<JIRA.Response<JIRA.Schema.FilterBean>>;
    getFilters(params: JIRA.Params.FilterGetFilters, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean[]>>): Promise<JIRA.Response<JIRA.Schema.FilterBean[]>>;
    getMyFilters(params: JIRA.Params.FilterGetMyFilters, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean[]>>): Promise<JIRA.Response<JIRA.Schema.FilterBean[]>>;
    getSharePermission(params: JIRA.Params.FilterGetSharePermission, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterPermissionBean>>): Promise<JIRA.Response<JIRA.Schema.FilterPermissionBean>>;
    getSharePermissions(params: JIRA.Params.FilterGetSharePermissions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterPermissionBean[]>>): Promise<JIRA.Response<JIRA.Schema.FilterPermissionBean[]>>;
    removeFilterAsFavorite(params: JIRA.Params.FilterRemoveFilterAsFavorite, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean>>): Promise<JIRA.Response<JIRA.Schema.FilterBean>>;
    resetColumns(params: JIRA.Params.FilterResetColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    searchForFilters(params: JIRA.Params.FilterSearchForFilters, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanFilterBean2>>): Promise<JIRA.Response<JIRA.Schema.PageBeanFilterBean2>>;
    setColumns(params: JIRA.Params.FilterSetColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setDefaultShareScope(params: JIRA.Params.FilterSetDefaultShareScope, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DefaultShareScopeBean>>): Promise<JIRA.Response<JIRA.Schema.DefaultShareScopeBean>>;
    updateFilter(params: JIRA.Params.FilterUpdateFilter, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.FilterBean>>): Promise<JIRA.Response<JIRA.Schema.FilterBean>>;
  };
  group: {
    addUserToGroup(params: JIRA.Params.GroupAddUserToGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Group>>): Promise<JIRA.Response<JIRA.Schema.Group>>;
    createGroup(params: JIRA.Params.GroupCreateGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Group>>): Promise<JIRA.Response<JIRA.Schema.Group>>;
    getGroup(params: JIRA.Params.GroupGetGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Group>>): Promise<JIRA.Response<JIRA.Schema.Group>>;
    getUsersFromGroup(params: JIRA.Params.GroupGetUsersFromGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanUserJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanUserJsonBean>>;
    removeGroup(params: JIRA.Params.GroupRemoveGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    removeUserFromGroup(params: JIRA.Params.GroupRemoveUserFromGroup, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  groups: {
    findGroups(params: JIRA.Params.GroupsFindGroups, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.GroupSuggestionsBean>>): Promise<JIRA.Response<JIRA.Schema.GroupSuggestionsBean>>;
  };
  groupuserpicker: {
    findUsersAndGroups(params: JIRA.Params.GroupuserpickerFindUsersAndGroups, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.UsersAndGroupsBean>>): Promise<JIRA.Response<JIRA.Schema.UsersAndGroupsBean>>;
  };
  issue: {
    addAttachment(params: JIRA.Params.IssueAddAttachment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AttachmentJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.AttachmentJsonBean[]>>;
    addComment(params: JIRA.Params.IssueAddComment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CommentJsonBean>>): Promise<JIRA.Response<JIRA.Schema.CommentJsonBean>>;
    addVote(params: JIRA.Params.IssueAddVote, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    addWatcher(params: JIRA.Params.IssueAddWatcher, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    addWorklog(params: JIRA.Params.IssueAddWorklog, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogJsonBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogJsonBean>>;
    assignIssue(params: JIRA.Params.IssueAssignIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    bulkDeleteIssueProperty(params: JIRA.Params.IssueBulkDeleteIssueProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    bulkIssueCreate(params: JIRA.Params.IssueBulkIssueCreate, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueCreateResponse>>): Promise<JIRA.Response<JIRA.Schema.IssueCreateResponse>>;
    bulkSetIssueProperty(params: JIRA.Params.IssueBulkSetIssueProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    createIssue(params: JIRA.Params.IssueCreateIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueCreateResponse>>): Promise<JIRA.Response<JIRA.Schema.IssueCreateResponse>>;
    createOrUpdateRemoteIssueLink(params: JIRA.Params.IssueCreateOrUpdateRemoteIssueLink, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.RemoteIssueLinkCreateOrUpdateResponse>>): Promise<JIRA.Response<JIRA.Schema.RemoteIssueLinkCreateOrUpdateResponse>>;
    deleteComment(params: JIRA.Params.IssueDeleteComment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssue(params: JIRA.Params.IssueDeleteIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssueProperty(params: JIRA.Params.IssueDeleteIssueProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteRemoteIssueLinkByGlobalId(params: JIRA.Params.IssueDeleteRemoteIssueLinkByGlobalId, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteRemoteIssueLinkById(params: JIRA.Params.IssueDeleteRemoteIssueLinkById, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteVote(params: JIRA.Params.IssueDeleteVote, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWatcher(params: JIRA.Params.IssueDeleteWatcher, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWorklog(params: JIRA.Params.IssueDeleteWorklog, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWorklogProperty(params: JIRA.Params.IssueDeleteWorklogProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    editIssue(params: JIRA.Params.IssueEditIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getChangeLogs(params: JIRA.Params.IssueGetChangeLogs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanChangeHistoryBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanChangeHistoryBean>>;
    getComment(params: JIRA.Params.IssueGetComment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CommentJsonBean>>): Promise<JIRA.Response<JIRA.Schema.CommentJsonBean>>;
    getComments(params: JIRA.Params.IssueGetComments, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PaginatedCommentsJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PaginatedCommentsJsonBean>>;
    getCreateIssueMetadata(params: JIRA.Params.IssueGetCreateIssueMetadata, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CreateMetaBean>>): Promise<JIRA.Response<JIRA.Schema.CreateMetaBean>>;
    getEditIssueMetadata(params: JIRA.Params.IssueGetEditIssueMetadata, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EditMetaBean>>): Promise<JIRA.Response<JIRA.Schema.EditMetaBean>>;
    getIssue(params: JIRA.Params.IssueGetIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueBean>>): Promise<JIRA.Response<JIRA.Schema.IssueBean>>;
    getIssuePickerSuggestions(params: JIRA.Params.IssueGetIssuePickerSuggestions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssuePickerResult>>): Promise<JIRA.Response<JIRA.Schema.IssuePickerResult>>;
    getIssueProperty(params: JIRA.Params.IssueGetIssueProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getIssuePropertyKeys(params: JIRA.Params.IssueGetIssuePropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    getIssueWatchers(params: JIRA.Params.IssueGetIssueWatchers, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WatchersBean>>): Promise<JIRA.Response<JIRA.Schema.WatchersBean>>;
    getIssueWorklogs(params: JIRA.Params.IssueGetIssueWorklogs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogWithPaginationBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogWithPaginationBean>>;
    getRemoteIssueLinkById(params: JIRA.Params.IssueGetRemoteIssueLinkById, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.RemoteIssueLinkBean>>): Promise<JIRA.Response<JIRA.Schema.RemoteIssueLinkBean>>;
    getRemoteIssueLinks(params: JIRA.Params.IssueGetRemoteIssueLinks, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.RemoteIssueLinkBean[]>>): Promise<JIRA.Response<JIRA.Schema.RemoteIssueLinkBean[]>>;
    getTransitions(params: JIRA.Params.IssueGetTransitions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TransitionsListBean>>): Promise<JIRA.Response<JIRA.Schema.TransitionsListBean>>;
    getVotes(params: JIRA.Params.IssueGetVotes, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VoteBean>>): Promise<JIRA.Response<JIRA.Schema.VoteBean>>;
    getWorklog(params: JIRA.Params.IssueGetWorklog, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogJsonBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogJsonBean>>;
    getWorklogProperty(params: JIRA.Params.IssueGetWorklogProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getWorklogPropertyKeys(params: JIRA.Params.IssueGetWorklogPropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    sendNotificationForIssue(params: JIRA.Params.IssueSendNotificationForIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setIssueProperty(params: JIRA.Params.IssueSetIssueProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setWorklogProperty(params: JIRA.Params.IssueSetWorklogProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    transitionIssue(params: JIRA.Params.IssueTransitionIssue, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    updateComment(params: JIRA.Params.IssueUpdateComment, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CommentJsonBean>>): Promise<JIRA.Response<JIRA.Schema.CommentJsonBean>>;
    updateRemoteIssueLink(params: JIRA.Params.IssueUpdateRemoteIssueLink, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    updateWorklog(params: JIRA.Params.IssueUpdateWorklog, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogJsonBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogJsonBean>>;
  };
  issueLink: {
    createIssueLink(params: JIRA.Params.IssueLinkCreateIssueLink, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssueLink(params: JIRA.Params.IssueLinkDeleteIssueLink, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getIssueLink(params: JIRA.Params.IssueLinkGetIssueLink, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueLinkJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueLinkJsonBean>>;
  };
  issueLinkType: {
    createIssueLinkType(params: JIRA.Params.IssueLinkTypeCreateIssueLinkType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>;
    deleteIssueLinkType(params: JIRA.Params.IssueLinkTypeDeleteIssueLinkType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getIssueLinkType(params: JIRA.Params.IssueLinkTypeGetIssueLinkType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>;
    getIssueLinkTypes(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueLinkTypesBean>>): Promise<JIRA.Response<JIRA.Schema.IssueLinkTypesBean>>;
    updateIssueLinkType(params: JIRA.Params.IssueLinkTypeUpdateIssueLinkType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueLinkTypeJsonBean>>;
  };
  issuesecurityschemes: {
    getIssueSecurityScheme(params: JIRA.Params.IssuesecurityschemesGetIssueSecurityScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SecuritySchemeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.SecuritySchemeJsonBean>>;
    getIssueSecuritySchemes(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SecuritySchemesJsonBean>>): Promise<JIRA.Response<JIRA.Schema.SecuritySchemesJsonBean>>;
  };
  issuetype: {
    createIssueType(params: JIRA.Params.IssuetypeCreateIssueType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>;
    deleteIssueType(params: JIRA.Params.IssuetypeDeleteIssueType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssueTypeProperty(params: JIRA.Params.IssuetypeDeleteIssueTypeProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllIssueTypesForUser(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeJsonBean[]>>;
    getAlternativeIssueTypes(params: JIRA.Params.IssuetypeGetAlternativeIssueTypes, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeJsonBean[]>>;
    getIssueType(params: JIRA.Params.IssuetypeGetIssueType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>;
    getIssueTypeProperty(params: JIRA.Params.IssuetypeGetIssueTypeProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getIssueTypePropertyKeys(params: JIRA.Params.IssuetypeGetIssueTypePropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    loadIssueTypeAvatar(params: JIRA.Params.IssuetypeLoadIssueTypeAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AvatarBean>>): Promise<JIRA.Response<JIRA.Schema.AvatarBean>>;
    setIssueTypeProperty(params: JIRA.Params.IssuetypeSetIssueTypeProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    updateIssueType(params: JIRA.Params.IssuetypeUpdateIssueType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeJsonBean>>;
  };
  jql: {
    convertUserIdentifiersToAccountIDsInJqlQueries(params: JIRA.Params.JqlConvertUserIdentifiersToAccountIDsInJqlQueries, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.JqlPersonalDataMigrationResponse>>): Promise<JIRA.Response<JIRA.Schema.JqlPersonalDataMigrationResponse>>;
    getFieldAutoCompleteSuggestions(params: JIRA.Params.JqlGetFieldAutoCompleteSuggestions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AutoCompleteResultWrapper>>): Promise<JIRA.Response<JIRA.Schema.AutoCompleteResultWrapper>>;
    getFieldReferenceData(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AutoCompleteResponseBean>>): Promise<JIRA.Response<JIRA.Schema.AutoCompleteResponseBean>>;
  };
  mypermissions: {
    getMyPermissions(params: JIRA.Params.MypermissionsGetMyPermissions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionsJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionsJsonBean>>;
  };
  mypreferences: {
    deleteLocale(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deletePreference(params: JIRA.Params.MypreferencesDeletePreference, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getLocale(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.LocaleBean>>): Promise<JIRA.Response<JIRA.Schema.LocaleBean>>;
    getPreference(params: JIRA.Params.MypreferencesGetPreference, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setLocale(params: JIRA.Params.MypreferencesSetLocale, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setPreference(params: JIRA.Params.MypreferencesSetPreference, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  myself: {
    getCurrentUser(params: JIRA.Params.MyselfGetCurrentUser, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User>>): Promise<JIRA.Response<JIRA.Schema.User>>;
  };
  notificationscheme: {
    getNotificationScheme(params: JIRA.Params.NotificationschemeGetNotificationScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.NotificationSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.NotificationSchemeBean>>;
    getNotificationSchemesPaginated(params: JIRA.Params.NotificationschemeGetNotificationSchemesPaginated, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanNotificationSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanNotificationSchemeBean>>;
  };
  permissions: {
    getAllPermissions(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionsJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionsJsonBean>>;
    getPermittedProjects(params: JIRA.Params.PermissionsGetPermittedProjects, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermittedProjectsBean>>): Promise<JIRA.Response<JIRA.Schema.PermittedProjectsBean>>;
  };
  permissionscheme: {
    createPermissionGrant(params: JIRA.Params.PermissionschemeCreatePermissionGrant, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionGrantBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionGrantBean>>;
    createPermissionScheme(params: JIRA.Params.PermissionschemeCreatePermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>;
    deletePermissionScheme(params: JIRA.Params.PermissionschemeDeletePermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deletePermissionSchemeEntity(params: JIRA.Params.PermissionschemeDeletePermissionSchemeEntity, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllPermissionSchemes(params: JIRA.Params.PermissionschemeGetAllPermissionSchemes, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemesBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemesBean>>;
    getPermissionScheme(params: JIRA.Params.PermissionschemeGetPermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>;
    getPermissionSchemeGrant(params: JIRA.Params.PermissionschemeGetPermissionSchemeGrant, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionGrantBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionGrantBean>>;
    getPermissionSchemeGrants(params: JIRA.Params.PermissionschemeGetPermissionSchemeGrants, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionGrantsBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionGrantsBean>>;
    updatePermissionScheme(params: JIRA.Params.PermissionschemeUpdatePermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>;
  };
  priority: {
    getPriorities(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PriorityJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.PriorityJsonBean[]>>;
    getPriority(params: JIRA.Params.PriorityGetPriority, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PriorityJsonBean>>): Promise<JIRA.Response<JIRA.Schema.PriorityJsonBean>>;
  };
  project: {
    addActorsToProjectRole(params: JIRA.Params.ProjectAddActorsToProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    assignPermissionScheme(params: JIRA.Params.ProjectAssignPermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>;
    createProject(params: JIRA.Params.ProjectCreateProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectIdentity>>): Promise<JIRA.Response<JIRA.Schema.ProjectIdentity>>;
    deleteActorsFromProjectRole(params: JIRA.Params.ProjectDeleteActorsFromProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteProject(params: JIRA.Params.ProjectDeleteProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteProjectAvatar(params: JIRA.Params.ProjectDeleteProjectAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteProjectProperty(params: JIRA.Params.ProjectDeleteProjectProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAccessibleProjectTypeByKey(params: JIRA.Params.ProjectGetAccessibleProjectTypeByKey, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectTypeBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectTypeBean>>;
    getAllProjectAvatars(params: JIRA.Params.ProjectGetAllProjectAvatars, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectAvatars>>): Promise<JIRA.Response<JIRA.Schema.ProjectAvatars>>;
    getAllProjects(params: JIRA.Params.ProjectGetAllProjects, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectBean[]>>): Promise<JIRA.Response<JIRA.Schema.ProjectBean[]>>;
    getAllProjectTypes(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectTypeBean[]>>): Promise<JIRA.Response<JIRA.Schema.ProjectTypeBean[]>>;
    getAllStatusesForProject(params: JIRA.Params.ProjectGetAllStatusesForProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeWithStatusJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeWithStatusJsonBean[]>>;
    getAssignedPermissionScheme(params: JIRA.Params.ProjectGetAssignedPermissionScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.PermissionSchemeBean>>;
    getProject(params: JIRA.Params.ProjectGetProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectBean>>;
    getProjectComponents(params: JIRA.Params.ProjectGetProjectComponents, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ComponentBean[]>>): Promise<JIRA.Response<JIRA.Schema.ComponentBean[]>>;
    getProjectComponentsPaginated(params: JIRA.Params.ProjectGetProjectComponentsPaginated, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanComponentWithIssueCountBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanComponentWithIssueCountBean>>;
    getProjectIssueSecurityLevels(params: JIRA.Params.ProjectGetProjectIssueSecurityLevels, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SecurityListLevelJsonBean>>): Promise<JIRA.Response<JIRA.Schema.SecurityListLevelJsonBean>>;
    getProjectIssueSecurityScheme(params: JIRA.Params.ProjectGetProjectIssueSecurityScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SecuritySchemeJsonBean>>): Promise<JIRA.Response<JIRA.Schema.SecuritySchemeJsonBean>>;
    getProjectNotificationScheme(params: JIRA.Params.ProjectGetProjectNotificationScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.NotificationSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.NotificationSchemeBean>>;
    getProjectProperty(params: JIRA.Params.ProjectGetProjectProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getProjectPropertyKeys(params: JIRA.Params.ProjectGetProjectPropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    getProjectRoleDetails(params: JIRA.Params.ProjectGetProjectRoleDetails, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean[]>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean[]>>;
    getProjectRoleForProject(params: JIRA.Params.ProjectGetProjectRoleForProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    getProjectRolesForProject(params: JIRA.Params.ProjectGetProjectRolesForProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getProjectsPaginated(params: JIRA.Params.ProjectGetProjectsPaginated, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanProjectBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanProjectBean>>;
    getProjectTypeByKey(params: JIRA.Params.ProjectGetProjectTypeByKey, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectTypeBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectTypeBean>>;
    getProjectVersions(params: JIRA.Params.ProjectGetProjectVersions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionBean[]>>): Promise<JIRA.Response<JIRA.Schema.VersionBean[]>>;
    getProjectVersionsPaginated(params: JIRA.Params.ProjectGetProjectVersionsPaginated, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanVersionBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanVersionBean>>;
    loadProjectAvatar(params: JIRA.Params.ProjectLoadProjectAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AvatarBean>>): Promise<JIRA.Response<JIRA.Schema.AvatarBean>>;
    setActorsForProjectRole(params: JIRA.Params.ProjectSetActorsForProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    setProjectAvatar(params: JIRA.Params.ProjectSetProjectAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setProjectProperty(params: JIRA.Params.ProjectSetProjectProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    updateProject(params: JIRA.Params.ProjectUpdateProject, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectBean>>;
    updateProjectType(params: JIRA.Params.ProjectUpdateProjectType, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectBean>>;
  };
  projectCategory: {
    createProjectCategory(params: JIRA.Params.ProjectCategoryCreateProjectCategory, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectCategoryBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectCategoryBean>>;
    deleteProjectCategory(params: JIRA.Params.ProjectCategoryDeleteProjectCategory, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllProjectCategories(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectCategoryBean[]>>): Promise<JIRA.Response<JIRA.Schema.ProjectCategoryBean[]>>;
    getProjectCategoryById(params: JIRA.Params.ProjectCategoryGetProjectCategoryById, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectCategoryBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectCategoryBean>>;
    updateProjectCategory(params: JIRA.Params.ProjectCategoryUpdateProjectCategory, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectCategoryJsonBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectCategoryJsonBean>>;
  };
  projectvalidate: {
    getValidProjectKey(params: JIRA.Params.ProjectvalidateGetValidProjectKey, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getValidProjectName(params: JIRA.Params.ProjectvalidateGetValidProjectName, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    validateProjectKey(params: JIRA.Params.ProjectvalidateValidateProjectKey, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ErrorCollection>>): Promise<JIRA.Response<JIRA.Schema.ErrorCollection>>;
  };
  resolution: {
    getResolution(params: JIRA.Params.ResolutionGetResolution, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ResolutionBean>>): Promise<JIRA.Response<JIRA.Schema.ResolutionBean>>;
    getResolutions(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ResolutionBean[]>>): Promise<JIRA.Response<JIRA.Schema.ResolutionBean[]>>;
  };
  role: {
    addDefaultActorsToProjectRole(params: JIRA.Params.RoleAddDefaultActorsToProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    createProjectRole(params: JIRA.Params.RoleCreateProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    deleteDefaultActorsFromProjectRole(params: JIRA.Params.RoleDeleteDefaultActorsFromProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteProjectRole(params: JIRA.Params.RoleDeleteProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    fullyUpdateProjectRole(params: JIRA.Params.RoleFullyUpdateProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    getAllProjectRoles(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean[]>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean[]>>;
    getDefaultActorsForProjectRole(params: JIRA.Params.RoleGetDefaultActorsForProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    getProjectRoleById(params: JIRA.Params.RoleGetProjectRoleById, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
    partialUpdateProjectRole(params: JIRA.Params.RolePartialUpdateProjectRole, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ProjectRoleBean>>): Promise<JIRA.Response<JIRA.Schema.ProjectRoleBean>>;
  };
  screens: {
    addFieldToDefaultScreen(params: JIRA.Params.ScreensAddFieldToDefaultScreen, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    addScreenTabField(params: JIRA.Params.ScreensAddScreenTabField, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableFieldBean>>): Promise<JIRA.Response<JIRA.Schema.ScreenableFieldBean>>;
    createScreenTab(params: JIRA.Params.ScreensCreateScreenTab, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableTabBean>>): Promise<JIRA.Response<JIRA.Schema.ScreenableTabBean>>;
    deleteScreenTab(params: JIRA.Params.ScreensDeleteScreenTab, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllScreens(params: JIRA.Params.ScreensGetAllScreens, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanScreenBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanScreenBean>>;
    getAllScreenTabFields(params: JIRA.Params.ScreensGetAllScreenTabFields, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableTabBean[]>>): Promise<JIRA.Response<JIRA.Schema.ScreenableTabBean[]>>;
    getAllScreenTabs(params: JIRA.Params.ScreensGetAllScreenTabs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableTabBean[]>>): Promise<JIRA.Response<JIRA.Schema.ScreenableTabBean[]>>;
    getAvailableScreenFields(params: JIRA.Params.ScreensGetAvailableScreenFields, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableFieldBean[]>>): Promise<JIRA.Response<JIRA.Schema.ScreenableFieldBean[]>>;
    moveScreenTab(params: JIRA.Params.ScreensMoveScreenTab, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    moveScreenTabField(params: JIRA.Params.ScreensMoveScreenTabField, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    removeScreenTabField(params: JIRA.Params.ScreensRemoveScreenTabField, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    updateScreenTab(params: JIRA.Params.ScreensUpdateScreenTab, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ScreenableTabBean>>): Promise<JIRA.Response<JIRA.Schema.ScreenableTabBean>>;
  };
  search: {
    searchForIssuesUsingJqlGet(params: JIRA.Params.SearchSearchForIssuesUsingJqlGet, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SearchResultsBean>>): Promise<JIRA.Response<JIRA.Schema.SearchResultsBean>>;
    searchForIssuesUsingJqlPost(params: JIRA.Params.SearchSearchForIssuesUsingJqlPost, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SearchResultsBean>>): Promise<JIRA.Response<JIRA.Schema.SearchResultsBean>>;
  };
  securitylevel: {
    getIssueSecurityLevel(params: JIRA.Params.SecuritylevelGetIssueSecurityLevel, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.SecurityLevelJsonBean>>): Promise<JIRA.Response<JIRA.Schema.SecurityLevelJsonBean>>;
  };
  serverInfo: {
    getJiraInstanceInfo(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ServerInfoBean>>): Promise<JIRA.Response<JIRA.Schema.ServerInfoBean>>;
  };
  session: {
    createSession(params: JIRA.Params.SessionCreateSession, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AuthSuccess>>): Promise<JIRA.Response<JIRA.Schema.AuthSuccess>>;
    deleteSession(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getSession(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.CurrentUser>>): Promise<JIRA.Response<JIRA.Schema.CurrentUser>>;
  };
  settings: {
    getIssueNavigatorDefaultColumns(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ColumnItem[]>>): Promise<JIRA.Response<JIRA.Schema.ColumnItem[]>>;
    setIssueNavigatorDefaultColumns(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  status: {
    getAllStatuses(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.StatusJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.StatusJsonBean[]>>;
    getStatus(params: JIRA.Params.StatusGetStatus, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.StatusJsonBean>>): Promise<JIRA.Response<JIRA.Schema.StatusJsonBean>>;
  };
  statuscategory: {
    getAllStatusCategories(params: JIRA.Params.Empty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.StatusCategoryJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.StatusCategoryJsonBean[]>>;
    getStatusCategory(params: JIRA.Params.StatuscategoryGetStatusCategory, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.StatusCategoryJsonBean>>): Promise<JIRA.Response<JIRA.Schema.StatusCategoryJsonBean>>;
  };
  task: {
    cancelTask(params: JIRA.Params.TaskCancelTask, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getTask(params: JIRA.Params.TaskGetTask, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.TaskProgressBeanObject>>): Promise<JIRA.Response<JIRA.Schema.TaskProgressBeanObject>>;
  };
  universal_avatar: {
    deleteAvatar(params: JIRA.Params.UniversalAvatarDeleteAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAvatars(params: JIRA.Params.UniversalAvatarGetAvatars, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AvatarsBean>>): Promise<JIRA.Response<JIRA.Schema.AvatarsBean>>;
    loadAvatar(params: JIRA.Params.UniversalAvatarLoadAvatar, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.AvatarBean>>): Promise<JIRA.Response<JIRA.Schema.AvatarBean>>;
  };
  user: {
    bulkGetUsers(params: JIRA.Params.UserBulkGetUsers, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanUser>>): Promise<JIRA.Response<JIRA.Schema.PageBeanUser>>;
    createUser(params: JIRA.Params.UserCreateUser, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User>>): Promise<JIRA.Response<JIRA.Schema.User>>;
    deleteUser(params: JIRA.Params.UserDeleteUser, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteUserProperty(params: JIRA.Params.UserDeleteUserProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    findUserKeysByQuery(params: JIRA.Params.UserFindUserKeysByQuery, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanUserKeyBean>>): Promise<JIRA.Response<JIRA.Schema.PageBeanUserKeyBean>>;
    findUsers(params: JIRA.Params.UserFindUsers, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User[]>>): Promise<JIRA.Response<JIRA.Schema.User[]>>;
    findUsersAssignableToIssues(params: JIRA.Params.UserFindUsersAssignableToIssues, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User[]>>): Promise<JIRA.Response<JIRA.Schema.User[]>>;
    findUsersAssignableToProjects(params: JIRA.Params.UserFindUsersAssignableToProjects, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User[]>>): Promise<JIRA.Response<JIRA.Schema.User[]>>;
    findUsersByQuery(params: JIRA.Params.UserFindUsersByQuery, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PageBeanUser>>): Promise<JIRA.Response<JIRA.Schema.PageBeanUser>>;
    findUsersForPicker(params: JIRA.Params.UserFindUsersForPicker, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.UserPickerResultsBean>>): Promise<JIRA.Response<JIRA.Schema.UserPickerResultsBean>>;
    findUsersWithBrowsePermission(params: JIRA.Params.UserFindUsersWithBrowsePermission, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User[]>>): Promise<JIRA.Response<JIRA.Schema.User[]>>;
    findUsersWithPermissions(params: JIRA.Params.UserFindUsersWithPermissions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User[]>>): Promise<JIRA.Response<JIRA.Schema.User[]>>;
    getUser(params: JIRA.Params.UserGetUser, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.User>>): Promise<JIRA.Response<JIRA.Schema.User>>;
    getUserDefaultColumns(params: JIRA.Params.UserGetUserDefaultColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.ColumnItem[]>>): Promise<JIRA.Response<JIRA.Schema.ColumnItem[]>>;
    getUserGroups(params: JIRA.Params.UserGetUserGroups, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.GroupJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.GroupJsonBean[]>>;
    getUserProperty(params: JIRA.Params.UserGetUserProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertyBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertyBean>>;
    getUserPropertyKeys(params: JIRA.Params.UserGetUserPropertyKeys, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>): Promise<JIRA.Response<JIRA.Schema.EntityPropertiesKeysBean>>;
    resetUserDefaultColumns(params: JIRA.Params.UserResetUserDefaultColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setUserDefaultColumns(params: JIRA.Params.UserSetUserDefaultColumns, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    setUserProperty(params: JIRA.Params.UserSetUserProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
  };
  version: {
    createVersion(params: JIRA.Params.VersionCreateVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionBean>>): Promise<JIRA.Response<JIRA.Schema.VersionBean>>;
    deleteAndReplaceVersion(params: JIRA.Params.VersionDeleteAndReplaceVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteVersion(params: JIRA.Params.VersionDeleteVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getVersion(params: JIRA.Params.VersionGetVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionBean>>): Promise<JIRA.Response<JIRA.Schema.VersionBean>>;
    getVersionsRelatedIssuesCount(params: JIRA.Params.VersionGetVersionsRelatedIssuesCount, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionIssueCountsBean>>): Promise<JIRA.Response<JIRA.Schema.VersionIssueCountsBean>>;
    getVersionsUnresolvedIssuesCount(params: JIRA.Params.VersionGetVersionsUnresolvedIssuesCount, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionUnresolvedIssueCountsBean>>): Promise<JIRA.Response<JIRA.Schema.VersionUnresolvedIssueCountsBean>>;
    mergeVersions(params: JIRA.Params.VersionMergeVersions, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    moveVersion(params: JIRA.Params.VersionMoveVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionBean>>): Promise<JIRA.Response<JIRA.Schema.VersionBean>>;
    updateVersion(params: JIRA.Params.VersionUpdateVersion, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.VersionBean>>): Promise<JIRA.Response<JIRA.Schema.VersionBean>>;
  };
  workflow: {
    createWorkflowTransitionProperty(params: JIRA.Params.WorkflowCreateWorkflowTransitionProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PropertyBean>>): Promise<JIRA.Response<JIRA.Schema.PropertyBean>>;
    deleteWorkflowTransitionProperty(params: JIRA.Params.WorkflowDeleteWorkflowTransitionProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getAllWorkflows(params: JIRA.Params.WorkflowGetAllWorkflows, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getWorkflowTransitionProperties(params: JIRA.Params.WorkflowGetWorkflowTransitionProperties, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PropertyBean[]>>): Promise<JIRA.Response<JIRA.Schema.PropertyBean[]>>;
    updateWorkflowTransitionProperty(params: JIRA.Params.WorkflowUpdateWorkflowTransitionProperty, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.PropertyBean>>): Promise<JIRA.Response<JIRA.Schema.PropertyBean>>;
  };
  workflowscheme: {
    createDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeCreateDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    createWorkflowScheme(params: JIRA.Params.WorkflowschemeCreateWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    deleteDefaultWorkflow(params: JIRA.Params.WorkflowschemeDeleteDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteDraftDefaultWorkflow(params: JIRA.Params.WorkflowschemeDeleteDraftDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssueTypesForWorkflowInDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteIssueTypesForWorkflowInDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteIssueTypesForWorkflowInWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteIssueTypesForWorkflowInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWorkflowForIssueTypeInDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteWorkflowForIssueTypeInDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWorkflowForIssueTypeInWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteWorkflowForIssueTypeInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    deleteWorkflowScheme(params: JIRA.Params.WorkflowschemeDeleteWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.Any>>): Promise<JIRA.Response<JIRA.Schema.Any>>;
    getDefaultWorkflow(params: JIRA.Params.WorkflowschemeGetDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DefaultBean>>): Promise<JIRA.Response<JIRA.Schema.DefaultBean>>;
    getDraftDefaultWorkflow(params: JIRA.Params.WorkflowschemeGetDraftDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.DefaultBean>>): Promise<JIRA.Response<JIRA.Schema.DefaultBean>>;
    getDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeGetDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    getIssueTypesForWorkflowsInDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeGetIssueTypesForWorkflowsInDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowMappingBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowMappingBean>>;
    getIssueTypesForWorkflowsInWorkflowScheme(params: JIRA.Params.WorkflowschemeGetIssueTypesForWorkflowsInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowMappingBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowMappingBean>>;
    getWorkflowForIssueTypeInDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeGetWorkflowForIssueTypeInDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeMappingBean>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeMappingBean>>;
    getWorkflowForIssueTypeInWorkflowScheme(params: JIRA.Params.WorkflowschemeGetWorkflowForIssueTypeInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.IssueTypeMappingBean>>): Promise<JIRA.Response<JIRA.Schema.IssueTypeMappingBean>>;
    getWorkflowScheme(params: JIRA.Params.WorkflowschemeGetWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    setIssueTypesForWorkflowInWorkflowScheme(params: JIRA.Params.WorkflowschemeSetIssueTypesForWorkflowInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    setWorkflowForIssueTypeInDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeSetWorkflowForIssueTypeInDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    setWorkflowForIssueTypeInWorkflowScheme(params: JIRA.Params.WorkflowschemeSetWorkflowForIssueTypeInWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    updateDefaultWorkflow(params: JIRA.Params.WorkflowschemeUpdateDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    updateDraftDefaultWorkflow(params: JIRA.Params.WorkflowschemeUpdateDraftDefaultWorkflow, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    updateDraftWorkflowScheme(params: JIRA.Params.WorkflowschemeUpdateDraftWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
    updateWorkflowScheme(params: JIRA.Params.WorkflowschemeUpdateWorkflowScheme, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>): Promise<JIRA.Response<JIRA.Schema.WorkflowSchemeBean>>;
  };
  worklog: {
    getIDsOfDeletedWorklogs(params: JIRA.Params.WorklogGetIDsOfDeletedWorklogs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogChangedSinceBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogChangedSinceBean>>;
    getIDsOfUpdatedWorklogs(params: JIRA.Params.WorklogGetIDsOfUpdatedWorklogs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogChangedSinceBean>>): Promise<JIRA.Response<JIRA.Schema.WorklogChangedSinceBean>>;
    getWorklogs(params: JIRA.Params.WorklogGetWorklogs, callback?: JIRA.Callback<JIRA.Response<JIRA.Schema.WorklogJsonBean[]>>): Promise<JIRA.Response<JIRA.Schema.WorklogJsonBean[]>>;
  };
}

export = JIRA;

export as namespace JIRA;
